### Introduction to Ansible

# Catalyst <!-- .element class="catalyst-logo" -->

Presented by [Travis Holton](#) <!-- .element: class="small-text"  -->



## Introduction to Ansible <!-- .slide: class="title-slide" --> <!-- .element: class="orange" -->


### Administrative stuff

* Bathrooms
* Fire exits



### Course outline

* [Automation & Infrastructure as Code](#/)
* [Types of Automation](#/4)
* [About Ansible](#/5)
* [Installing Ansible](#/6)
* [Ansible Basics](#/7)


* [Ad hoc Ansible](#/8)
* [Playbooks](#/9)
* [Variables](#/10)

* [Ansible Templates](#/11)
* [Iteration and Conditionals](#/12)
* [Conditionals](#/13)
* [Ansible Vault](#/14)
* [Filters](#/15)


* [Handlers](#/16)
* [Roles](#/17)
* [Best practices](#/19)
* [Wrap Up](#/20)



### Automation and Infrastructure as Code


#### Perspective
* Cattle vs Pets
    * Baker, Bill. (2012). _Scaling SQL Server_
    * Bias, Randy. (2014). _The Cloud Revolution_. [https://www.slideshare.net/randybias/the-cloud-revolution-cyber-press-forum-philippines](https://www.slideshare.net/randybias/the-cloud-revolution-cyber-press-forum-philippines)
* Manual processes vs automation


#### Pets
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Type of stand alone host or infrastructure
* <!-- .element: class="fragment" data-fragment-index="1" -->
  _Manually_ maintained
    * provisioned
    * configuration
    * deploy code
    * security patching
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Uptime measured in _months_ or _years_
* <!-- .element: class="fragment" data-fragment-index="3" -->
  Often have names (as pets usually do)
    * `asterix.mycompany.com`
    * `obelix.mycompany.com`


#### Cattle
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Arrays of servers
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Built using automation tools
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Uptime varies but often on much shorter scale (minutes to days)
* <!-- .element: class="fragment" data-fragment-index="3" -->
  Machine-generated naming convention
    * `i-abcde123456`
    * `ec-3-10-45-7.amazon.com`


#### Downsides of Pets
* Manual processes
* The _snowflake_ problem


#### Manual Processes
* Always possibility of error in any repetitive tasks
  - human or automated
* Humans are more prone to error than automated systems
* Probability of error increases as
  - procedure becomes more tedious
  - complexity increases


#### Problems with Pets
* Manually maintained
    * humans are error prone
* Over time hosts become cluttered
    * old deployments
    * stale dependencies
    * configuration drift
* Eventually evolve into _snowflakes_


#### Snowflake
* _Pet_ host that has become _one of a kind_
* Difficult or impossible to replicate host exactly
    * horizontal scaling
    * testing _in situ_ scenarios
    * recover from failure
* Become single point of failure for an organisation


#### Automation to the rescue

Automation is about taking manual processes and placing technology around them to make them repeatable.


#### Benefits of Automation

* Scalability
* Reliability
* Repeatability


#### Infrastructure as Code (IaC)
* All aspects of infrastructure should be managed by automation tools
* _Recipes_ or _playbooks_ for automation tools reside in code
* Should be under version control just like application code


#### Benefits of IaC
* Auditability
* Security
* Compliance



### Types of Automation


#### Types of Automation

* Configuration Management <!-- .element: class="fragment" data-fragment-index="0" -->
* Provisioning <!-- .element: class="fragment" data-fragment-index="1" -->
* Application Deployment <!-- .element: class="fragment" data-fragment-index="2" -->
* Security & compliance <!-- .element: class="fragment" data-fragment-index="3" -->
* Orchestration <!-- .element: class="fragment" data-fragment-index="4" -->


#### Configuration Management

* <!-- .element: class="fragment" data-fragment-index="0" -->
  Often synonymous with _automation_
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Determining the state that servers/applications should be in and making sure conditions are met
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Agentless vs Agents
* <!-- .element: class="fragment" data-fragment-index="3" -->
  Often declarative syntax with DSL


#### Configuration Management Tools

* <!-- .element: class="fragment" data-fragment-index="0" -->
  [Salt](https://saltstack.com)
* <!-- .element: class="fragment" data-fragment-index="1" -->
  [Puppet](https://puppetlabs.com)
* <!-- .element: class="fragment" data-fragment-index="2" -->
  [Chef](https://www.chef.io)
* <!-- .element: class="fragment" data-fragment-index="3" -->
  [Ansible](https://www.ansible.com)


#### Provisioning

* <!-- .element: class="fragment" data-fragment-index="0" -->The act of creating a server, network router, loadbalancer, etc. from some type of image
* <!-- .element: class="fragment" data-fragment-index="1" -->Can be on bare metal or in cloud hosted virtualisation
* <!-- .element: class="fragment" data-fragment-index="2" -->Example: Installation of centos image on a virtual host


#### Provisioning Tools

* <!-- .element: class="fragment" data-fragment-index="0" -->
  Red Hat Kickstart
* <!-- .element: class="fragment" data-fragment-index="1" -->
  [FAI](http://fai-project.org/)
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Debian Preseed
* <!-- .element: class="fragment" data-fragment-index="3" -->
  Cobbler
* <!-- .element: class="fragment" data-fragment-index="4" -->
  [Ansible](https://www.ansible.com)


#### Application Deployment

* Take an artefact of development <!-- .element: class="fragment" data-fragment-index="0" -->
  * Libraries <!-- .element: class="fragment" data-fragment-index="1" -->
  * Executable code <!-- .element: class="fragment" data-fragment-index="2" -->
* Place on platform of choice <!-- .element: class="fragment" data-fragment-index="3" -->
* Make sure it runs <!-- .element: class="fragment" data-fragment-index="4" -->


#### Application Deployment Tools

* <!-- .element: class="fragment" data-fragment-index="0" -->
  [Fabric](https://www.fabfile.org/)
* <!-- .element: class="fragment" data-fragment-index="1" -->
  [Capistrano](https://capistranorb.com/)
* <!-- .element: class="fragment" data-fragment-index="2" -->
  [GoCD](https://thoughtworks.com/go)
* <!-- .element: class="fragment" data-fragment-index="3" -->
  [Ansible](https://www.ansible.com)


#### Orchestration

* Concerned with automating workflows/processes
* Interconnecting components of an application
  * webservers
  * databases
  * message queues
  * application
* Make sure components function together as a unit


#### Orchestration tools

* [AWS CloudFormation](https://aws.amazon.com/cloudformation/)
* [OpenStack Heat](https://wiki.openstack.org/wiki/Heat)
* [Hashicorp](https://www.terraform.io/)
* [Red Hat CloudForms](https://www.redhat.com/en/technologies/cloud-computing/cloudforms)
* Docker
  * [Kubernetes](https://kubernetes.io)
  * [Swarm](https://docs.docker.com/engine/swarm)
* [Ansible](https://www.ansible.com)


#### Security & Compliance

* Usually driven by some legislative or regulatory law
  * Designed to prevent unwanted disclosure, alteration or destruction of sensitive information
  * Code subject to audit
* PCI or government security standard compliance
* Automation removes complexity of managing security in systems


#### Security & Compliance Tools

* [InSpec (Ruby)](https://www.inspec.io)
* [Goss (Go)](https://github.com/aelsabbahy/goss)
* [testinfra (Python)](https://testinfra.readthedocs.io/en/latest)
* [Ansible](https://www.ansible.com)


#### Ansible is a tool for:

* Configuration Management
* Deploying software
* Orchestration
* Provisioning
* Auditability


#### Ansible features

* Based on Python
* Agentless (only needs Python on remote host)
* Only requires SSH



### Installing Ansible


#### Installing Ansible

* Target latest stable Ansible version
* [Official Documentation](http://docs.ansible.com/ansible/latest/intro_installation.html)
* Explore a couple approaches using:
  * OS package manager
  * Python package manager (pip)


#### Base Requirements

* To run Ansible you will need
  * <!-- .element: class="fragment" data-fragment-index="0" -->A computer
    * Should run on all modern operating systems
  * <!-- .element: class="fragment" data-fragment-index="1" -->Python &ge;3.5


#### Installing OS package
* <!-- .element: class="fragment" data-fragment-index="0" -->It should be
  possible to install Ansible using the package manager of your OS.
* <!-- .element: class="fragment" data-fragment-index="1" -->Ansible available with OS typically a bit out of date
   ```
    apt-cache policy ansible
    ansible:
    Installed: (none)
    Candidate: 2.9.6+dfsg-1
    Version table:
        2.9.6+dfsg-1 500
            500 https://ubuntu.catalyst.net.nz/ubuntu focal/universe amd64 Packages
   ```
* <!-- .element: class="fragment" data-fragment-index="2" -->Generally necessary to add source repo ([apt, yum, etc.])


#### Installing as Python virtualenv
* Common installation method is to use a Python <!-- .element: class="fragment" data-fragment-index="0" -->*virtual environment*
* <!-- .element: class="fragment" data-fragment-index="1" -->Advantage of this technique: multiple separate ansible environments
  * Different needs for different projects


#### Ansible in a Python Virtual Environment
* Install python requirements
   ```bash
   sudo apt-get install python3-pip virtualenv
   ```
  <!-- .element: style="font-size:11pt;"  -->
* Set up local Python environment
   ```bash
   python3 -m venv ~/venv
   ```


#### Install Ansible
* <!-- .element: class="fragment" data-fragment-index="0" -->Activate virtualenv as base of Python interpreter
   ```bash
   source ~/venv/bin/activate
   ```
* <!-- .element: class="fragment" data-fragment-index="1" -->Update Python package manager (pip)
   ```bash
   pip install -U pip
   ```
* <!-- .element: class="fragment" data-fragment-index="2" -->Use Python package manager to install Ansible
   ```bash
   pip install ansible
   ```


#### Ansible release cycle

* It is worth keeping up to date with Ansible [releases](https://docs.ansible.com/ansible/latest/release_and_maintenance.html)
* Use pip or a PPA to get a recent version
* Ansible is developed and released on a flexible 4 months release cycle
* Ansible supports the two most recent major stable releases



### Ansible Basics


#### Terminology

<div style="width:50%;float:left;">
  <dl>
    <dt>Task</dt>
    <dd>An action to perform</dd>
    <dt>Play</dt>
    <dd>A collection of tasks</dd>
    <dt>Playbook</dt>
    <dd>YAML file containing one or more plays</dd>
  </dl>
</div>

![Workflow](img/ansible-workflow.png "Ansible Workflow")
<!-- .element: class="" style="width:50%;float:left" -->


#### More Terminology

<dl>
  <dt>Module</dt>
  <dd>Blob of Python code which is executed to perform task</dd>
  <dt>Inventory</dt>
  <dd>File containing hosts and groups of hosts to run tasks</dd>
  <dt>Collection</dt>
  <dd>Packaging and distribution format for Ansible code</dd>
</dl>


#### Collections
* <!-- .element: class="fragment" data-fragment-index="0" -->Introduced in 2020 as part of restructuring of Ansible release process
* <!-- .element: class="fragment" data-fragment-index="1" -->Ansible core separated from modules
* 3 part naming for all <!-- .element: class="fragment" data-fragment-index="2" -->**modules**, **roles** and **plugins**
  - namespace
  - collection
  - module, role or plugin name



### Running Ansible


### Setup environment
* Before we begin the lessons we need to do a few things
* <!-- .element: class="fragment" data-fragment-index="0" -->Location of
  code for examples and lessons
* <!-- .element: class="fragment" data-fragment-index="1" -->Set up virtual
  host to interact with
* <!-- .element: class="fragment" data-fragment-index="2" -->Tell Ansible how
  to reach virtual host(s)


#### Source code
* For the lesson <!-- .element: class="fragment" data-fragment-index="0" -->today __`$INTRO_ANSIBLE_DIR`__ is the path to the __`introduction-to-ansible/sample-code`__ directory
   ```bash
   $ echo $INTRO_ANSIBLE_DIR
   /home/train/introduction-to-ansible/sample-code
   ```
* <!-- .element: class="fragment" data-fragment-index="1" -->All code examples and lessons can be found in this directory


#### Simulate remote hosts

![Vagrant-vm](img/one-vagrant-vm.png "Vagrant VM") <!-- .element:  style="height:50%;" -->

* <!-- .element: class="fragment" data-fragment-index="0" style="font-size: 20pt;" -->
  For this course we will be using [Vagrant](https://www.vagrantup.com/intro/index.html)
  to simulate remote hosts
* <!-- .element: class="fragment" data-fragment-index="1" style="font-size: 20pt;" -->
  Instance(s) use a centos 7 image
<pre class="fragment" data-fragment-index="2" style="font-size:13pt;"><code data-trim>
    $ cd $INTRO_ANSIBLE_DIR/adhoc
    $ vagrant up
    .

    ==&gt; default: Machine booted and ready!
</code></pre>


### Defining your infrastructure


#### Ansible Inventory
* The <!-- .element: class="fragment" data-fragment-index="0" -->_inventory_ is how Ansible views your infrastructure
* <!-- .element: class="fragment" data-fragment-index="1" -->Inventory identifies hosts that Ansible can manage
* <!-- .element: class="fragment" data-fragment-index="2" -->Can be a text file in one of many formats
   - ini
   - yaml
   - json
   - toml
* <!-- .element: class="fragment" data-fragment-index="3" -->Can also be dynamic
   - Cloud API


#### Sample Inventory File
* Example inventory file using <code>ini</code> format
* Defines 6 hosts
* One host per line...
* or use `[x:y]` to represent multiple hosts

```ini
web1.mycompany.com ansible_host=152.240.43.12 opt2=arg2
web2.mycompany.com

db1.mycompany.com

app[1:2].mycompany.com
loadbalancer.mycompany.com
```


#### Sample Inventory file
* Same as previous example but in <code>yaml</code>

```
---
all:
  hosts:
    web1.mycompany.com:
        ansible_host: 152.240.43.12
        opt2: arg2
    web2.mycompany.com:
    db1.mycompany.com:
    loadbalancer.mycompany.com:
    app[1:2].mycompany.com
```


#### Inventory and Groups
* <!-- .element: class="fragment" data-fragment-index="0" -->Ansible groups are an abstraction for managing infrastructure
* Use set logic to represent infrastructure <!-- .element: class="fragment" data-fragment-index="1" -->![basic-groups](img/ansible-basic-groups.drawio.png "Groups overview")<!-- .element: class="img-right" style="width:40%;float:right;" -->
* <!-- .element: class="fragment" data-fragment-index="2" -->Groups can overlap or even be nested in other groups as needed


#### Grouping Hosts

<div style="width:50%;float:left;">
  <ul>
    <li class="fragment" data-fragment-index="0">
      In ini format <code>[sections]</code> used to organise hosts into <em>groups</em>
    </li>
    <ul>
      <li class="fragment" data-fragment-index="0">Functional roles</li>
      <li class="fragment" data-fragment-index="0">Separate regions</li>
    </ul>
  </ul>
</div>

```ini
# sample inventory
[web]
web1.mycompany.com
web2.mycompany.com

[app]
app[1:3].mycompany.com # <- bracket shortcut

[wellington]
jump
web1.mycompany.com
app1.mycompany.com

[auckland]
web2.mycompany.com
app[2:3].mycompany.com

[ci]
app3.mycompany.com
```
<!-- .element: class="fragment" data-fragment-index="1" style="float: left; width: 50%; font-size: 13pt;" -->


#### Grouping Hosts

* Using yaml format <!-- .element: class="fragment" data-fragment-index="0" -->_children_ used to organise hosts into groups
   ```
   ---
   all:
     children:
       web:
         hosts:
           web1.mycompany.com:
           web2.mycompany.com:
       app:
         hosts:
           app[1:3].mycompany.com: # <- bracket shortcut
       wellington:
         hosts:
           jump
           web1.mycompany.com:
           app1.mycompany.com:
       auckland:
         hosts:
           web2.mycompany.com:
           app[2:3].mycompany.com:
    ```


#### Running Ansible

* There are two ways to run ansible:<!-- .element: class="fragment" data-fragment-index="0" -->
  * ad-hoc <!-- .element: class="fragment" data-fragment-index="1" -->
    * Run a single task <!-- .element: class="fragment" data-fragment-index="2" -->
    * <!-- .element: class="fragment" data-fragment-index="3" --><code>ansible &lt;pattern&gt; [options]</code>
  * Playbook <!-- .element: class="fragment" data-fragment-index="4" -->
    * Run multiple tasks sequentially <!-- .element: class="fragment" data-fragment-index="5" -->
    * <!-- .element: class="fragment" data-fragment-index="6" --><code>ansible-playbook &lt;pattern&gt; [options]</code>


#### Ad-hoc tasks with Ansible

`ansible <pattern> [options]`

* Perform a few <!-- .element: class="fragment" data-fragment-index="0" -->_ad-hoc_ operations with Ansible
  * Check connection to server<!-- .element: class="fragment" data-fragment-index="1" -->
  * Install packages<!-- .element: class="fragment" data-fragment-index="2" -->
  * Run system commands <!-- .element: class="fragment" data-fragment-index="3" -->


#### Running ad-hoc commands with Ansible

<code>ansible </code><code style="color:red;">&lt;host pattern&gt;</code><code> [OPTIONS]</code>

* _host pattern_ can be:
   * the name of a specific host in inventory
   * a <em>group</em> of hosts from the inventory

| Option   | Argument  | Description |
|---- | ----- | ---- |
|-m  | string | module name to execute; default to _command_ module |
| -a | string | arguments to module |
| -i | string | path to inventory file |
| -b |   | Privilege escalation |
| --become-method | string | which become method to use; default is _sudo_ |
<!-- .element: style="font-size:12pt;"  -->


##### Exercise: Ping remote host

* [ansible.builtin.ping](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/ping_module.html) is an Ansible module that just checks whether or not Ansible can create a SSH sessions with hosts
* Use the <!-- .element: class="fragment" data-fragment-index="1" -->_ping_  module to check if our host accepts SSH connections
    ```bash
    $ ansible myserver -m ansible.builtin.ping
    ```
    <!-- .element: class="fragment" data-fragment-index="1" -->
    ```bash
    [WARNING]: No inventory was parsed, only implicit localhost is available
    [WARNING]: provided hosts list is empty, only localhost is available. Note that
    the implicit localhost does not match 'all'
    ```
    <!-- .element: class="fragment" data-fragment-index="2" -->
* <!-- .element: class="fragment" data-fragment-index="3" -->Ansible doesn't know anything about infrastructure at the moment.


#### Connecting Ansible to a Host
* Ansible works by creating an SSH connection with remote hosts <!-- .element: class="fragment" data-fragment-index="0" -->
* Need to tell Ansible how to connect to our host via SSH <!-- .element: class="fragment" data-fragment-index="1" -->
* <!-- .element: class="fragment" data-fragment-index="2" -->This can be
  configured in the inventory file


#### Our first inventory file

```bash
cat $INTRO_ANSIBLE_DIR/adhoc/hosts
```

* <!-- .element: class="fragment" data-fragment-index="0" -->
  Our inventory file specifies a single remote host: called _myserver_
* To connect to our Vagrant VM we need to set some special connection variables <!-- .element: class="fragment" data-fragment-index="1" -->
  * `ansible_host`
  * `ansible_user`
  * `ansible_port`
  * `ansible_private_key_file`
* Variables specified on same line as host <!-- .element: class="fragment" data-fragment-index="2" -->


##### Exercise: Edit your inventory file to fill in missing connection information
* <!-- .element: class="fragment" data-fragment-index="0" -->
  First you need to find the connection information
    ```bash
    $ vagrant ssh-config
    Host default
        HostName ???
        User ???
        Port ???
        .
        IdentityFile ???
        .
    ```
* <!-- .element: class="fragment" data-fragment-index="1" -->
 Use the values from your host to fill in missing arguments in `hosts` file

    <details>
    <summary>
    Solution
    </summary>

    ```
    myserver ansible_host=127.0.0.1 ansible_port=2222 ansible_user=vagrant  ansible_private_key_file=.vagrant/machines/default/virtualbox/private_key
    ```
    <!-- .element: style="font-size:10pt;"  -->

    </details>


##### Exercise: Try the ping command again

* <!-- .element: class="fragment" data-fragment-index="0" -->
   Now that you have updated `hosts` with the connection info you can try to
   run the `ping` command again.
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Use `-i` argument to tell Ansible to use the inventory file 
    ```bash
    $ ansible myserver -i hosts -m ansible.builtin.ping
    ```
    <!-- .element: class="fragment" data-fragment-index="2" -->
    ```bash
    myserver | SUCCESS => {
        "ansible_facts": {
            "discovered_interpreter_python": "/usr/bin/python"
        },
        "changed": false,
        "ping": "pong"
    }
    ```
    <!-- .element: class="fragment" data-fragment-index="3" style="font-color:green;" -->


#### Behind the scenes
  * Create SSH connection to a host or list of hosts (group) in parallel
  * Copy a small blob of executable code to each remote machine
  * Performs task: execute the code; capturing return code and output
  * Removes the blob of code
  * Closes the SSH connection
  * Report back on outcome of task


#### The `ansible.cfg` File

<pre class="fragment" data-fragment-index="0" style="font-size:13pt;"><code data-trim>
# sample ansible.cfg

[defaults]
inventory = hosts
remote_user = vagrant
private_key_file = .vagrant/machines/default/virtualbox/private_key
host_key_checking = False
</code></pre>

* A configuration file where you can provide some defaults for ansible<!-- .element: class="fragment" data-fragment-index="0" -->
* <!-- .element: class="fragment" data-fragment-index="1" -->
  `.ini` syntax <!-- .element: class="fragment" data-fragment-index="1" -->
* Specify SSH connection, which inventory file to use <!-- .element: class="fragment" data-fragment-index="2" -->


#### Locating `ansible.cfg`

* 4 possible locations in order of priority
  * <!-- .element: class="fragment" data-fragment-index="0" -->
    File specified by `ANSIBLE_CONFIG`
  * <!-- .element: class="fragment" data-fragment-index="1" -->
    Current directory (`./ansible.cfg`)
  * <!-- .element: class="fragment" data-fragment-index="2" -->
    Home directory (`~/.ansible.cfg`)
  * <!-- .element: class="fragment" data-fragment-index="3" -->
    `/etc/ansible/ansible.cfg`


##### Exercise: Set up `ansible.cfg` and modify inventory file

```
cat $INTRO_ANSIBLE_DIR/ansible.cfg.sample
```

<pre class="fragment" data-fragment-index="0"><code data-trim>

cd $INTRO_ANSIBLE_DIR/adhoc
cp ../ansible.cfg.sample ansible.cfg
cat $INTRO_ANSIBLE_DIR/adhoc/hosts
myserver ansible_host=127.0.0.1 ansible_port=2222
</code></pre>

You now no longer need to specify an inventory file <!-- .element: class="fragment" data-fragment-index="1" -->
<pre class="fragment" data-fragment-index="1"><code data-trim> $ ansible myserver -m ping</code></pre>


#### Ansible Modules

* <!-- .element: class="fragment" data-fragment-index="0" style="font-size: 18pt;" -->
  Blob of Python that performs a very specific action
* <!-- .element: class="fragment" data-fragment-index="1" style="font-size: 18pt;" -->
  Ship with Ansible
* <!-- .element: class="fragment" data-fragment-index="2" style="font-size: 18pt;" -->
  More than 4400 Ansible modules
  ```bash
  ansible-doc -l
  ```


#### How Ansible works
* <!-- .element: class="fragment" data-fragment-index="0" style="font-size: 18pt;" -->
  When you run Ansible:
  * <!-- .element: class="fragment" data-fragment-index="1" style="font-size: 18pt;" -->
    `scp` to temporary directory on target host
  * <!-- .element: class="fragment" data-fragment-index="2" style="font-size: 18pt;" -->
    Execute python on target host
  * <!-- .element: class="fragment" data-fragment-index="3" style="font-size: 18pt;" -->
    Returns JSON
  * <!-- .element: class="fragment" data-fragment-index="4" style="font-size: 18pt;" -->
    Code removed from target host
  * <!-- .element: class="fragment" data-fragment-index="5" style="font-size: 18pt;" -->
    Local Ansible evaluates returned JSON
  * <!-- .element: class="fragment" data-fragment-index="6" style="font-size: 18pt;" -->
    Output for task based on returned values


#### Ansible Output
* <!-- .element: class="fragment" data-fragment-index="0" -->
  The output for each module/task tells you the net effect of task execution
   + `ok` <!-- .element: style="color:green;"  -->: no change was made
   + `changed` <!-- .element: style="color:orange;"  -->: A change was made to
     target
   + `failed` <!-- .element: style="color:red;"  -->: Ansible was unable to
     change target
* <!-- .element: class="fragment" data-fragment-index="1" -->
   Exact interpretation will depend on task and module implementation


#### [`ansible.builtin.command`](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/command_module.html)

<code style="font-size:15pt;">ansible &lt;host pattern&gt; -m ansible.builtin.command -a &lt;args to command&gt;</code>

* <!-- .element: class="fragment" data-fragment-index="0" -->
  Execute arbitrary commands (eg. `bash`) on a system
* <!-- .element: class="fragment" data-fragment-index="1" -->
   Pass the command you want to run with the `-a` argument
   ```bash
   ansible myserver -m ansible.builtin.command -a 'ls /'
   ```
* <!-- .element: class="fragment" data-fragment-index="2" -->
  The default module
* <!-- .element: class="fragment" data-fragment-index="3" -->
  These are equivalent:
     ```bash
     ansible myserver -m ansible.builtin.command -a uptime
     ```
     ```bash
     ansible myserver -a uptime
     ```


##### Exercise: Get tail of system messages on your vagrant host

* Use the _command_ modules to tail the system log file
* This will be `/var/log/messages` on centos

<pre class="fragment" data-fragment-index="0"><code data-trim>
    ansible myserver  -b -a "tail /var/log/messages"
</code></pre>


##### Note about the _command_ module
* <!-- .element: class="fragment" data-fragment-index="0" -->
  `ansible.builtin.command` is a useful module to know about for quick one-off
  tasks.
* <!-- .element: class="fragment" data-fragment-index="1" -->
  However, it really should **_only_** be used as a last resort
* <!-- .element: class="fragment" data-fragment-index="2" -->
  As a best practice one should use module made for specific task
  - creating/deleting a file
  - system management tasks
  - managing cloud infrastructure
* <!-- .element: class="fragment" data-fragment-index="3" -->
  There are several thousand modules to choose from
  


##### Exercise: install a package
* <!-- .element: class="fragment" data-fragment-index="0" -->
   Let's install `mtr` (a network monitoring tool) on our host
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Tempting to use the _command_ module...**but don't!**
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Better choice on a centos platform is the [ansible.builtin.yum](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/yum_module.html) module
   * <!-- .element: class="fragment" data-fragment-index="3" -->
      Specific options
      ```bash
      -b -m ansible.builtin.yum
      ```
      <!-- .element: class="fragment" data-fragment-index="4" -->
      ```bash
      -a "name=mtr state=present"
      ```
      <!-- .element: class="fragment" data-fragment-index="5" -->
   * <!-- .element: class="fragment" data-fragment-index="6" -->
     Complete command
      ```bash
       ansible myserver -b -m ansible.builtin.yum -a "name=mtr state=present"
      ```
      <!-- .element: style="font-size:10pt;" -->


#### Module documentation

Places you can find module documentation <!-- .element: class="fragment" data-fragment-index="0" -->

* <!-- .element: class="fragment" data-fragment-index="1" " -->
  Online search
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Inline documentation

<pre class="fragment" data-fragment-index="2" style="font-size: 10pt;"><code data-trim>
$ ansible-doc yum

&gt; YUM    (~/venv/local/lib/python2.7/site-packages/ansible/modules/packaging/os/yum.py)

        Installs, upgrade, downgrades, removes, and lists packages and groups with the `yum' package manager.

OPTIONS (= is mandatory):

- allow_downgrade
        Specify if the named package and version is allowed to downgrade a maybe already installed
        higher version of that package. Note that setting allow_downgrade=True can make this module
        behave in a non-idempotent way. The task could end up with a set of packages that does not
        match the complete list of specified packages to install (because dependencies between the
        downgraded package and others can cause changes to the packages which were in the earlier
        transaction).
        (Choices: yes, no)
        [Default: no]
        version_added: 2.4
</code></pre>


#### Summary

* This section has given you a small taste of using Ansible in _ad-hoc_ mode
* Interacted with a remote server
  * Check if SSH works
  * Install OS packages
  * Start services
* Explore documentation with `ansible-doc`


#### Before we move on

```bash
vagrant halt
vagrant destroy
```



### Playbooks


#### Ansible Playbook

* Primary means of:
  * Configuration management
  * Deploying applications
  * Provisioning
  * Orchestration
* A sequence of tasks


#### Ansible Playbooks

```bash
cd $INTRO_ANSIBLE_DIR/working-with-playbooks
```
```
.
├── ansible.cfg
├── hosts
├── static-site.yml
├── templates
│   ├── index.html.j2
│   └── mysite.conf.j2
└── Vagrantfile
```

| Name  | Type  | Description |
|--- | ---- |   ----- |
| `static-site.yml` | file  | Ansible playbook |
| files        | directory | Artefacts to be placed on remote host |
| templates    | directory | Templates that will be rendered and uploaded to remote host |


#### Ansible Playbook Structure

<div style="width:50%;float:left;">
    <img src="img/playbook-anatomy.svg"/>
</div>

<div style="width:50%;float:left;">
  <ul>
    <li class="fragment" data-fragment-index="0">
      A playbook is a YAML file containing a list of <em>plays</em>
    </li>
    <li class="fragment" data-fragment-index="1">
      A play is a dictionary object
    </li>
    <ul>
      <li class="fragment" data-fragment-index="1">
        <code style="color:red;">key</code><code>: </code><code style="color:blue;">value</code>
      </li>
    </ul>
    <li class="fragment" data-fragment-index="2">
      Some keys in a play may contain dictionaries or lists
    </li>
  </ul>
</div>


#### Ansible Playbook Structure

* A play must contain:
  * `hosts`
    * A string representing a particular host or _group_ of hosts
      * `hosts: localhost`
      * `hosts: app.mywebsite.com`
      * `hosts: appserver`
    * These are what you will configure


#### Ansible Playbook Structure

* A play may optionally contain:
  * tasks
    * A list of dictionaries
    * What you want to do
  * name
    * Description of the play
  * vars
    * Variables scoped to the play


#### Structure of a Task

* A task is a dictionary object containing
  * name
    * Describes what the task does
    * Optional but best practice to use
  * module
    * Dictionary object
    * Key represents Python module which will perform tasks
    * May have arguments


#### Structure of a Task
<div style="width:50%;float:left;">
    <img src="img/playbook-task-anatomy.svg"/>
</div>
<div style="width:50%;float:left;">
    <ul>
        <li>
            Two styles of module object in tasks
            <ul>
                <li>string form</li>
                <li>dictionary form</li>
            </ul>
        </li>
        <li>
            Dictionary form is more suitable for complex arguments
        </li>
        <li>
            Matter of preference/style
        </li>
    </ul>
</div>


#### Ansible YAML basic formatting
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Tabs not allowed
* <!-- .element: class="fragment" data-fragment-index="1" -->
2 space indentation
* <!-- .element: class="fragment" data-fragment-index="2" -->Playbook indentation
  <pre style="font-size:10pt;"><code data-trim data-noescape>
  - name: This is a play
  <mark>  </mark>hosts: somehosts             # 2 spaces
  <mark>  </mark>tasks:
  <mark style="background-color:lightblue">    </mark>- name: This is a task     # 4 spaces
  <mark style="background-color:lightgreen">      </mark>module:                  # 6 spaces
  <mark style="background-color:pink">        </mark>attr: someattr         # 8 spaces
  <mark style="background-color:pink">        </mark>attr1: someattr
  <mark style="background-color:pink">        </mark>attr2: someattr
</code></pre>
* <!-- .element: class="fragment" data-fragment-index="3" -->Task file indentation
  <pre style="font-size:10pt;"><code data-trim data-noescape>
  - name: This is a task
  <mark>  </mark>collection.namespace.module:              # 2 spaces
  <mark style="background-color:lightblue">    </mark>attr1: value1               # 4 spaces
  <mark style="background-color:lightblue">    </mark>attr2: value2               # 4 spaces
  <mark style="background-color:lightblue">    </mark>attr3: value3               # 4 spaces
  <mark>  </mark>loop: "{{ list_of_items }}"   # 2 spaces
</code></pre>
* <!-- .element: class="fragment" data-fragment-index="4" -->Use an editor that supports YAML syntax


#### Editing Ansible YAML
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Important to choose a configurable editor
  * Space indentation
  * syntax highlighting
  * built-in linting a bonus
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Installed for you on PC
  * vim
  * emacs
  * Atom


#### Example Vim config

```
# ~/.vim/after/ftplugin/yaml.vim
set tabstop=2 "Indentation levels every two columns

set expandtab "Convert all tabs that are typed to spaces
set shiftwidth=2 "Indent/outdent by two columns
set shiftround "Indent/outdent to nearest tabstop
```


#### Create a new VM

```bash
vagrant up
```

* This creates an Ubuntu VM
* Additional ports for a static website
  * 80 (HTTP)
  * 443 (HTTPS)


#### Deploy and configure an application

![install](img/ansible-nginx-install.svg "Ansible Install nginx")
* `static-site.yml` performs following actions
   + Installs nginx package
   + Templates our nginx config (`templates/mysite.conf.j2`)
   + Renders template file (<code>templates/index.html.j2</code>) and place it on host
   + Re/Starts nginx


#### Run our first playbook

* Run playbook
  ```bash
  ansible-playbook static-site.yml
  ```
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Visit the [static site](http://localhost:8080) once playbook has finished.


#### Privilege Escalation
* <!-- .element: class="fragment" data-fragment-index="0" style="font-size: 20pt;" -->
  Some tasks we need to perform require sudo privileges on target machine
* <!-- .element: class="fragment" data-fragment-index="1" style="font-size: 20pt;" -->
The _become_ attribute tells Ansible to run tasks as a certain user
   + By default this is _sudo_
   + Specify user with `become_user`

   <pre style="font-size:16pt;"><code class="ini" data-trim data-noescape>
    - name: Set up static website with nginx
      hosts: myserver
      <mark >become: true</mark>
      tasks:
   </code></pre>
* <!-- .element: class="fragment" data-fragment-index="2" style="font-size: 20pt;" -->
   Can be in _play_ or _task_ scope
* <!-- .element: class="fragment" data-fragment-index="3" style="font-size: 20pt;" -->
   Accepts  _yes_ or _true_ (_no_ or _false_ are implicit)


#### Ansible Abstraction Layer
* <!-- .element: class="fragment" data-fragment-index="0" -->
  We have used both `yum` and `apt` to install packages in previous exercises, respectively
* <!-- .element: class="fragment" data-fragment-index="1" -->
  There is also a [ansible.builtin.package]("https://docs.ansible.com/ansible/latest/collections/ansible/builtin/package_module.html") module for abstracting OS specific package managers
  - ansible.builtin.yum
  - ansible.builtin.apt
* <!-- .element: class="fragment" data-fragment-index="2" -->
  For _language_ specific packages need to use specific modules
  - [community.general.npm](https://docs.ansible.com/ansible/latest/collections/community/general/npm_module.html)
  - [ansible.builtin.pip](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/pip_module.html)


####  Idempotent Behaviour
* <!-- .element: class="fragment" data-fragment-index="0" -->
  It is sometimes necessary to run configuration/deploy tools more than once
   + Debugging/Iteration
   + Temporary issues (eg. loss of network)
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Can be problematic if operations not prepared to manage repetition
   + Unmanaged duplication
   + Errors caused by violating unique constraints


#### Idempotent Behaviour
* Most Ansible modules aim to be <!-- .element: class="fragment" data-fragment-index="0" -->_idempotent_
* Can be run repeatedly and will only change <!-- .element: class="fragment" data-fragment-index="1" -->_once_
* Output of each task indicates when target action resulted in a change <!-- .element: class="fragment" data-fragment-index="2" -->
   + `ok` <!-- .element: style="color:green;"  -->: Already in desired state;
     Ansible did not change anything
   + `changed` <!-- .element: style="color:orange;"  -->: Ansible changed to
     desired state


##### Exercise: Idempotent Behaviour
* Log into your VM and remove some files
   ```bash
   vagrant ssh
   sudo rm -f /usr/share/nginx/html/index.html /etc/nginx/sites-available/mysite.conf
   ```
   <!-- .element: style="font-size:10pt;"  -->
*  <!-- .element: class="fragment" data-fragment-index="0" -->
   Run the `static-site-fail.yml` playbook
   ```
   ansible-playbook stati-site-fail.yml
   ```
   * <!-- .element: class="fragment" data-fragment-index="1" -->
     Note that one task has _changed_ <!-- .element: style="color:orange"  -->
*  <!-- .element: class="fragment" data-fragment-index="2" -->
    Remove _fail_ task and run again
   * <!-- .element: class="fragment" data-fragment-index="3" -->
     The task that had changed earlier should now be _ok_<!-- .element: style="color:green"  -->


#### Summary

* Use Ansible Playbook to run complex tasks
* Playbook consists of array of YAML dictionaries called <em>plays</em>
* Each play contains array of tasks that are executed on remote host



### Variables in Ansible


#### Variables in Ansible

* Several different types:
  * Inventory variables
  * Play scoped variables
  * Task variables
  * Extra variables


#### Inventory Variables

* Scoped to a host throughout the execution of a playbook
* Assigned in
  * The inventory file
  * Specific subdirectories
    * host_vars/_host_
    * group_vars/_group_
* Can be referenced directly when executing play on host
* Available in _hostvars_ dictionary


#### Variables in Inventory File
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Assign variable to a single host
* <!-- .element: class="fragment" data-fragment-index="1" --> 
   ini version
    <pre style="font-size:10pt;" ><code class="ini" data-trim data-noescape>
    [web]
    web1.mycompany.com <mark>ansible_host=152.240.43.12 opt2=arg2</mark>
    web2.mycompany.com
    .
    </code></pre>
* <!-- .element: class="fragment" data-fragment-index="2" --> 
   yaml version
    <pre style="font-size:10pt;" ><code class="ini" data-trim data-noescape>
    ---
    all:
      children:
        web:
          hosts:
            web1.mycompany.com
              <mark>ansible_host: 152.240.43.12</mark>
              <mark>opt2: arg2</mark>
            web2.mycompany.com
    </code></pre>


#### Variables in Inventory File
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Assign variables to groups of hosts 
* <!-- .element: class="fragment" data-fragment-index="1" -->
   INI version
    <pre style="font-size:10pt;" ><code class="ini" data-trim data-noescape>
    [web]
    web1.mycompany.com
    web2.mycompany.com
    <mark>[web:vars]
    proxy=web.mycompany.com
    </mark>
    </code></pre>
* <!-- .element: class="fragment" data-fragment-index="2" -->
  yaml version
    <pre style="font-size:10pt;" ><code class="ini" data-trim data-noescape>
    ---
    all:
      children:
        web:
          hosts:
            web1.mycompany.com
            web2.mycompany.com
        <mark>vars:
          proxy: web.mycompany.com</mark>
    </code></pre>


#### Using the `host_vars` directory

* <!-- .element: class="fragment" data-fragment-index="0" style="font-size: 20pt;" -->
  Ansible will automatically load files under `host_vars` directory
  * <!-- .element: class="fragment" data-fragment-index="1" style="font-size: 20pt;" -->
    Files with same name as particular host
  * <!-- .element: class="fragment" data-fragment-index="2" style="font-size: 20pt;" -->
    Any files in a directory with the same name as a host
    <pre  class="fragment" data-fragment-index="3"><code data-trim data-noescape>
      [web]
      web1.myhost.com
      web2.myhost.com
    </code></pre>
    <pre  class="fragment" data-fragment-index="4"><code data-trim data-noescape>
      $INTRO_ANSIBLE_DIR/working-with-playbooks
      └── host_vars
      <mark  class="fragment" data-fragment-index="5">    ├── web1.myhost.com.yml
          └── web2.myhost.com
              └── vars.yml</mark>
    </code></pre>

_.yml_ or _.yaml_ suffix is optional. Files can also be JSON (_.json_)
<!-- .element: class="fragment" data-fragment-index="6" style="font-size: 20pt;" -->


##### Exercise: create host inventory

* Create inventory variables for _myserver_ host
* Try either way: <!-- .element: class="fragment" data-fragment-index="0" -->
<pre style="width:100%;"  class="fragment" data-fragment-index="1"><code data-trim>
     cd $INTRO_ANSIBLE_DIR/working-with-playbooks
     mkdir -p host_vars
     $EDITOR host_vars/myserver.yml
</code></pre>
<pre style="width:100%;"  class="fragment" data-fragment-index="2"><code data-trim>
     mkdir -p host_vars/myserver
     $EDITOR host_vars/myserver/vars.yml
</code></pre>
<pre style="width:100%;"  class="fragment" data-fragment-index="3"><code data-trim>
    # variables for myserver
    ---
    foo: bar
</code></pre>


#### Using `group_vars`

* Similar to `host_vars` except for groups
  * Files with same name as particular group <!-- .element: class="fragment" data-fragment-index="1" -->
  * Any files in a directory with the same name as a group <!-- .element: class="fragment" data-fragment-index="2" -->
    <pre  class="fragment" data-fragment-index="3"><code data-trim data-noescape>
    [web]
    web1.myhost.com
    [app]
    app1.myhost.com
    </code></pre>
    <pre  class="fragment" data-fragment-index="4"><code data-trim data-noescape>
    $INTRO_ANSIBLE_DIR/working-with-playbooks
    └── group_vars
    <mark  class="fragment" data-fragment-index="5">    ├── web.yml
        └── app
            └── vars.yml</mark>
    </code></pre>


##### Exercise: Create `group_vars` for our app

* Create a `group_vars` directory and variable file
<pre style="width:100%;"  class="fragment" data-fragment-index="0"><code data-trim>
    cd $INTRO_ANSIBLE_DIR/working-with-playbooks/
    mkdir -p group_vars
    $EDITOR group_vars/web.yml
    </code></pre>
<pre style="width:100%;"  class="fragment" data-fragment-index="1"><code data-trim>
    mkdir -p group_vars/web
    $EDITOR group_vars/web/vars.yml
    </code></pre>
<pre style="width:100%;"  class="fragment" data-fragment-index="2"><code data-trim>
    # variables for "web" group
    ---
    bizz: buzz
</code></pre>


#### Using inventory variables

* Inventory variables available to Ansible throughout playbook run

```html
$ less $INTRO_ANSIBLE_DIR/working-with-playbooks/templates/index.html.j2
.
.
{% if foo is defined %}
<p>
The value for foo is <em>{{ foo }}</em>
</p>
{% endif %}
{% if bizz is defined %}
<p>
The value for bizz is <em>{{ bizz }}</em>
</p>
{% endif %}
.
.
```
<!-- .element: style="font-size: 14pt;" -->


#### Using Inventory Variables

```
ansible-playbook static-site.yml
```

* Run the ansible playbook again
* Note behaviour of _Copy up static website html_ task
* Reload [website](http://localhost:8000) when finished


#### `hostvars`

* Dictionary with variables from each host (including inventory variables)
* Indexed on hostname from inventory
  * `hostvars['myserver']['foo'] ==  hostvars.myserver.foo` <!-- .element: style="font-size:15pt;"  -->
* Can be accessed across plays in the same playbook run


#### Using `hostvars`
* Have a look at `use-hostvars.yml`
* Run `static-site.yml` and `use-hostvars.yml`
   ```
   ansible-playbook static-site.yml use-hostvars.yml
   ```
* Change <code style="color:red;">foo</code> to <code style="color:red;">hostvars.myserver.foo</code> in <code>playbook-localhost.yml</code> and run playbooks


##### Exercise: View a group variable from hostvars

* The bizz variable still appears as undefined in playbook run
* How would you find that in hostvars?
* A group variable will be defined for every host in a group

```
- name: Output of bizz
  ansible.builtin.debug:
    var: hostvars.myserver.bizz
```
<!-- .element: class="fragment" data-fragment-index="0" -->


#### Inventory groups and variables
* <!-- .element: class="fragment" data-fragment-index="0" -->
  All hosts implicitly belong to group called _all_
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Variables defined in following will be assigned in all inventory hosts
  - `group_vars/all.yml`
  - `group_vars/all/vars.yml`
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Inventory variable precedence (increasing order)
  - `group_vars/all`
  - `group_vars/*`
  - `host_vars/*`


#### Inventory variable scope
* Example inventory
  ```ini
  [web]
  web1.myhost.com foo=bar
  web2.myhost.com
  [app]
  app1.myhost.com
  app2.myhost.com
  [wellington]
  web1.myhost.com
  app1.myhost.com
  [auckland]
  web2.myhost.com
  app2.myhost.com
  ```
  <!-- .element: style="font-size:10pt;" class="fragment" data-fragment-index="0"  -->
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Example variable files
  ```
  # group_vars/all/vars.yml
  bizz: boooze
  foo: boo
  ```
  <!-- .element: style="font-size:10pt;" class="fragment" data-fragment-index="2"   -->
  ```
  # group_vars/app/vars.yml
  bizz: buzz
  ```
  <!-- .element: style="font-size:10pt;"  class="fragment" data-fragment-index="3"  -->
  ```
  # host_vars/app2.myhost.com.yml
  bizz: 123
  ```
  <!-- .element: style="font-size:10pt;"  class="fragment" data-fragment-index="4"  -->

<!-- .element: width="20%" style="float:left;" -->

![Inventory variable](img/ansible-group-venn.png "Group set variable definitions") <!-- .element: width="40%" class="fragment" data-fragment-index="5" -->


#### Play-scoped Variables

* Declared in control section of a play
  * vars - static declaration
  * vars_prompt - interactively prompt user when running playbook
* Can be referenced directly while in play


#### Play-scoped Variables

* Open `static-site.yml` in editor
* Edit as shown:
* Rerun ansible-playbook

<pre style="font-size:15pt;"  ><code data-trim data-noescape>
- name: Set up static website with nginx
  hosts: myserver
  become: true
<mark>  vars_prompt:
    - name: "participant_name"
      prompt: "What is your name"
      private: no
  vars:
    base_path: "/"
    course: Introduction to ansible
    lesson:
      name: Working with play variables
      session: 1</mark>
  tasks:
  .
  .
</code></pre>


#### Task Variables

* Contain data discovered while executing a task
* Gathering facts phase of play execution
* Tasks that gather user input
* Become part of the host scope once defined
* In a play they can be directly referenced
* In subsequent plays are available in hostvars dictionary for host


#### Assigning Variables with `set_fact`

```
tasks:
  - name: Assign an arbitrary variable
    ansible.builtin.set_fact:
      person_name: Homer Simpson

  - name: Assign dictionary
    ansible.builtin.set_fact:
      person_details:
        street: 754 Evergreen Terrace
        city: Springfield
        state: ST
```


#### Assigning Variables with `register`

```
- name: Execute code and capture the output
  ansible.builtin.command: 
    cmd: uptime
  register: uptime_output

  .
  .
- name: do something with output of uptime
  debug:
    var: uptime_output.stdout
```


##### Exercise: Assign Task Variables
<pre class="fragment" data-fragment-index="0"><code data-trim>
    tasks:
        - name: set name of person
          ansible.builtin.set_fact:
            person_name: Homer Simpson

        - name: Get output of uptime
          ansible.builtin.command:
            cmd: uptime
          register: uptime_output
        .
        .
</code></pre>

* Use <!-- .element: class="fragment" data-fragment-index="1" -->`set_fact` and `register` in `static-site.yml` to assign task variables
* Rerun ansible playbook <!-- .element: class="fragment" data-fragment-index="2" -->


#### Gathering facts

* Plugin run  before play executes
  * `ansible.builtin.setup` module
  * Collects detailed information about host and attaches to host scope
  * You can get an idea by running
    *  `ansible myserver -m ansible.builtin.setup`
  * Filter facts to find variables you are interested in:
     ```
     ansible myserver -m ansible.builtin.setup -a 'filter=ansible_distribution*' 
     ```
     <!-- .element: style="font-size:12pt;"  -->


##### Exercise: Display System Facts

* Uncomment area in `templates/index.html.j2`
* Re-run ansible-playbook and visit [static website](http://localhost:8080)

```
<!--   remove line
{% if ansible_distribution is defined %}
    <p>
    System facts: {{ ansible_distribution }} {{ ansible_distribution_major_version }} {{ ansible_architecture }}
    </p>
{% endif %}
-->   remove line
```
<!-- .element: style="font-size:10pt;"  -->


##### Exercise: Turn off fact gathering
* Edit `static-site.yml` as follows
* Re-run ansible-playbook and visit [static website](http://localhost:8080)

<pre><code data-trim data-noescape>
    - name: Set up static website with nginx
      hosts: myserver
      become: true
      <mark>gather_facts: false</mark>
      tasks:
        .
        .
</code></pre>


#### Extra Variables

* Variables passed to ansible or ansible-playbook via command line <!-- .element: class="fragment" data-fragment-index="0" -->
  * <!-- .element: class="fragment" data-fragment-index="1" --><code style="font-size:15pt;">ansible-playbook --extra-vars="key1=value1 key2=value2" playbook.yml</code>
  *  <!-- .element: class="fragment" data-fragment-index="2" --><code style="font-size:15pt;">ansible-playbook -e key1=value1 -e key2=value2 playbook.yml</code>
  * <!-- .element: class="fragment" data-fragment-index="3" --><code style="font-size:15pt;">ansible-playbook -e @extra-variables.yml playbook.yml</code>


#### Variable Precedence

*  Ansible variables in order of increasing precedence:
  * inventory file
  * host facts
  * play vars_files
  * task vars
  * include_vars
  * set_facts/registered variables
  * extra vars
* <!-- .element: class="fragment" data-fragment-index="0" -->
  [Complete list](https://docs.ansible.com/ansible/latest/playbooks_variables.html#variable-precedence-where-should-i-put-a-variable)


##### Exercise: Variable Precedence

* Pass _extra variables_ to ansible playbook to override previous examples
* For example you can create a new file called <!-- .element: class="fragment" data-fragment-index="1" -->`override-vars.yml`
   <pre  class="fragment" data-fragment-index="1"><code data-trim data-noescape>
     person_name: Marge Simpson
     lesson:
       name: Override variables
       session: 2
</code></pre>
* <!-- .element: class="fragment" data-fragment-index="2" -->Then add this on the command line
   ```bash
   ansible-playbook static-site.yml -e @override-vars.yml
   ```


#### Summary

* There are several types of variables in Ansible
  * inventory
  * play scoped
  * task variables
  * extra


<!--
### Ansible templates


#### Templating in Ansible

* Sometimes you need dynamic behaviour in plays/tasks
  * File locations
  * Fill in prompted information
  * Secret info (passwords, API keys)
  * Generally sourced from a variable
* Ansible uses a template syntax called Jinja2


#### Jinja2

* Templating language
* Provides tools for
  * Passing variables
  * Evaluating expressions
  * etc.
* References:
  * [Templating in Ansible](https://docs.ansible.com/ansible/latest/playbooks_templating.html)
  * [Jinja2 official docs](http://jinja.pocoo.org/docs/2.10/templates/)


#### Variables

* Simple variables or expressions
  * <code style="font-size:15pt;">{{ person_name  }}</code>
  * <code style="font-size:15pt;">{{ 1 + 1 }}</code>
* Dictionaries
  * <code style="font-size:15pt;">{{ lesson['name'] }}</code>
  * <code style="font-size:15pt;">{{ lesson.name }}</code>
  * <code style="font-size:15pt;">{{ hostvars['myserver'].ansible_distribution }}</code>


##### Exercise: Templating in our playbook

* Edit `vars` section in `static-site.yml` to add:
  * <code style="font-size:15pt;">static_file_directory</code>
  * <code style="font-size:15pt;">nginx_conf_directory</code>

```
vars:
  .
  static_file_directory: /usr/share/nginx/html
  nginx_available_conf: /etc/nginx/sites-available
tasks:
```


##### Exercise: Templating in our playbook

* Use template syntax to replace path for
  * nginx
  * index.html

<pre class="fragment" data-fragment-index="0"><code data-trim>
 - name: Template in nginx config file
   template:
     .
     dest: "{{ nginx_available_conf }}/mysite.conf"
     .

 - name: Copy up static website html
   template:
     .
     dest: "{{ static_file_directory }}/index.html"
     .
</code></pre>

Re-run the ansible playbook [> .element: class="fragment" data-fragment-index="1" <]


#### Templates and Quoting

* Be aware that you will often need to put quotes around templated elements
   ```
   dest: "{{ nginx_conf_directory }}/mysite.conf"
   ```
* However sometimes you do not need them
   ```
   command: ls {{ nginx_conf_directory }}
   ```-->



### Processing Data
#### Filters


#### Filters
```
cd $INTRO_ANSIBLE_DIR/filters
.
├── ansible.cfg
├── data.csv
├── hosts
├── playbook-formatting.yml
├── playbook-list.yml
├── filter-variables.yml
├── playbook-lookup.yml
└── undefined.yml
```


#### Filters
* Tools for manipulating datatypes
   * formatting data
   * boolean tests
   * iteration (i.e. with loop)
* [Offical documentation](https://docs.ansible.com/ansible/latest/user_guide/playbooks_filters.html) provides good overview
* A subset of functionality provided by Jinja2


#### Using filters

* Filter syntax:
   * <!-- .element: class="fragment" data-fragment-index="0" --><code style="color:blue;">variable</code><code style="color:red;"> | </code><code style="color:blue;">filtername</code>
   *  <!-- .element: class="fragment" data-fragment-index="1" --><code>variable</code> contains data of some kind
   *  <!-- .element: class="fragment" data-fragment-index="2" -->The "<code>|</code>" is the <em>pipe</em> symbol
* Filters will <!-- .element: class="fragment" data-fragment-index="3" -->_usually_ be used within Jinja2 expression syntax
   *  <!-- .element: class="fragment" data-fragment-index="4" --><code style="color:green;">{{</code><code style="color:blue;">  data </code><code> | </code><code style="color:blue;">filter</code><code style="color:green;"> }}</code>


#### `ansible-console`
* <!-- .element: class="fragment" data-fragment-index="0" -->
  We will use `ansible-console` to experiment with filters
  ```
  ansible-console -e @filter-variables.yml localhost
  ```
* <!-- .element: class="fragment" data-fragment-index="1" -->
  List all variables for host
  ```
  debug var=hostvars.localhost
  ```
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Use `debug` to  view a couple variables that are defined in
  `filter-variables.yml`
  ```
  debug var=list1
  ```
  ```
  debug var=security_groups
  ```


#### Simple list operations
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Reverse a list
  ```
  debug var='list1 | reverse'
  ```
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Combine 2 lists, element by element
  ```
  debug var='list1 | zip(list2)'
  ```
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Create a cartesian product of 2 lists
  ```
  debug var='list1 | product(list2)'
  ```
* <!-- .element: class="fragment" data-fragment-index="3" -->
  Find common elements in 2 lists (i.e. set intersection)
  ```
  debug var='list1 | intersect(list2)'
  ```


#### Simple dictionary operations
* <!-- .element: class="fragment" data-fragment-index="0" -->
  View contents of dictionary
  ```
  debug var=dict1
  ```
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Convert a dictionary to list of key value pairs
  ```
  debug var='dict1 | dict2items'
  ```
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Combine 2 dictionaries
  ```
  debug var='dict1 | combine({"a": "test"})'
  ```


#### Group set operations
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Apply set operations to groups
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Hosts that are in the _wellington_ **and** _web_ groups
  ```
  debug var='groups.wellington | intersect(groups.web)'
  ```
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Hosts that are in the _app_ group **but not in** _wellington_
  ```
  debug var='groups.app | difference(groups.wellington)'
  ```


#### Random useful tools
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Validate an IP address
  ```
  debug var='validip | ipv4'
  ```
  ```
  debug var='invalidip | ipv4'
  ```
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Extract address part of IP from CIDR
  ```
  debug var='security_groups.2.remote_ip_address | ipaddr("address")'
  ```
  <!-- .element: style="font-size:12pt;"  -->
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Extract filename from path
  ```
  debug var='basefilename | basename'
  ```


#### More handy filters
* Processing data
  * map
  * select & reject
* Formatting output
  * to_json
  * to_yaml
  * to_nice_json
  * to_nice_yaml
* And many more.


#### Summary
* Filters are a set of tools for
  * Processing string/text data
  * Processing complex data
* [Official Documentation](https://docs.ansible.com/ansible/latest/playbooks_filters.html)



### Iteration and Conditionals


#### Iteration and Conditionals

```
$ cd $INTRO_ANSIBLE_DIR/working-with-playbooks
.
├── ansible.cfg
├── hosts
├── playbook-dict.yml
├── playbook-lists.yml
```


### Iterating through lists


#### `loop`

* <!-- .element: class="fragment" data-fragment-index="0" -->
   Step through a list
* <!-- .element: class="fragment" data-fragment-index="1" --> 
   Each element in iteration is assigned to _item_ variable

<pre class="fragment" data-fragment-index="2"><code data-trim data-noescape>
 vars:
  list_of_files:
    - file1.txt
    - file2.txt
       .
       .
  tasks:
   - name: Process a list of items
     copy:
       src: "/path/to/local/{{ <mark class="fragment" data-fragment-index="1">item</mark> }}"
       dest: "/path/to/remote/{{ <mark class="fragment" data-fragment-index="1">item</mark> }}"
     <mark class="fragment" data-fragment-index="1">loop: "{{ list_of_files }}"</mark>
</code></pre>


#### Configuring our fake application _myapp_

* Have a look at <!-- .element: class="fragment" data-fragment-index="0" -->`playbook-lists.yml`
* Run the playbook and then have a look in <!-- .element: class="fragment" data-fragment-index="1" -->`/etc/myapp/config`
* Playbook adds lines to a configuration file for a fake app <!-- .element: class="fragment" data-fragment-index="2" -->
* There are a number of things we can optimise <!-- .element: class="fragment" data-fragment-index="3" -->


##### Exercise: Remove repetitive *allowed_host* tasks from playbook

* Use `loop` to refactor tasks
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Replace all allowed host tasks with a single iterative task

  ```yaml
  - name: Add allowed_hosts entries
    ansible.builtin.lineinfile:
      path: /etc/myapp/config
      line: "allow_from: {{ item }}"
      insertafter: EOF
    loop:
      - "{{ allowed_host1 }}"
      - "{{ allowed_host2 }}"
      - "{{ allowed_host3 }}"
  ```


##### Exercise: Remove repetitive listen tasks from playbook

* Use `loop` to refactor more tasks
* Replace all the listen tasks with:
   <pre class="fragment" data-fragment-index="0"><code data-trim>
    - name: Add listen port entries
      lineinfile:
        path: /etc/myapp/config
        line: "listen: {{ item }}"
        insertafter: EOF
      loop: "{{ listen_ports }}"
    </code></pre>


#### Complex iteration
* <!-- .element: class="fragment" data-fragment-index="0" -->
  You can  perform complex iteration by combining `loop` with filters
  ```
  - name: Process data structure
    some.module.name:
      attr: "{{ item.1 }}"
      path: "{{ item }}"
    loop: "{{ somevariable | somefilter }}"
  ```
|Variable type | filter |Description|
| ---  | ---- | --- |
| `dictionary_obj` | `dict2items` | Iterate through key/value pairs of `dictionary_obj` |
| `list1` | `product(list2)` | Create cartesian product of 2 lists|
| `list2` | `zip(list2)` | Step-wise iteration of `list_one` and `list_two` by index |
| `nested_list` | `flatten` | Flattens nested list(s) before iterating as a list |
<!-- .element: class="fragment" data-fragment-index="2" style="font-size:12pt;" -->


#### Complex Iteration

* Example loop over two lists at the same time
   ```
  vars:
    allowed_hosts:
      - "{{ allowed_host1 }}"
      - "{{ allowed_host2 }}"
      - "{{ allowed_host3 }}"

   - name: Add allowed ports for each host
     lineinfile:
       path: /etc/myapp/config
       line: "host: {{ item.0 }}:{{ item.1 }}"
       insertafter: EOF
     loop: "{{ listen_ports | product(allowed_hosts) }}"

   ```
   <!-- .element: style="font-size:10pt;"  -->


#### Iterating through dictionaries
<pre><code data-trim data-noescape>
- name: Task that iterates over a dictionary
  somemodule:
    arg: <mark class="fragment" data-fragment-index="2">"{{ item.key }}"</mark>
    arg2: <mark class="fragment" data-fragment-index="3">"{{ item.value }}"</mark>
  <mark class="fragment" data-fragment-index="0">loop: "{{ dictionary | dict2items }}"</mark>
</code></pre>
* Takes a dictionary argument <!-- .element: class="fragment" data-fragment-index="0" -->
* Each item has <!-- .element: class="fragment" data-fragment-index="1" -->
  * Key <!-- .element: class="fragment" data-fragment-index="2" -->
  * Value <!-- .element: class="fragment" data-fragment-index="3" -->

<!-- .element: class="stretch"  -->


#### More configuration for myapp

* <!-- .element: class="fragment" data-fragment-index="0" -->
  `playbook-dict.yml` sets up database config for our fake application
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Adds each element of *database* variable to config
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Run the playbook and have a look at `/etc/myapp/app_config`
  ```
  ansible-playbook playbook-dict.yml
  ```


##### Exercise: Remove repetition from playbook-dict.yml

* Refactor the playbook using `loop` and `dict2items` filter to remove repetitive tasks

<pre class="fragment" data-fragment-index="0"><code data-trim>
 - name: Add database config to /etc/myapp/config
   lineinfile:
     path: /etc/myapp/app_config
     line: "database_{{ item.key }}={{ item.value }}"
     regexp: '^database_{{ item.key}}.*'
   loop: "{{ database[env_name] | dict2items }}"
</code></pre>


#### `loop` vs `with_` iterators
* <!-- .element: class="fragment" data-fragment-index="0" -->
  The `loop` keyword was added in Ansible 2.5
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Replaces the `with_` iterators
  * `with_items`
  * `with_dict`
  * `with_together`
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Older Ansible might still use `with_*` iterators
* <!-- .element: class="fragment" data-fragment-index="3" -->
  For most applications `loop` is now the _recommended_ syntax
* <!-- .element: class="fragment" data-fragment-index="4" -->
  See [Ansible docs](https://docs.ansible.com/ansible/latest/user_guide/playbooks_loops.html#migrating-from-with-x-to-loop) for comparison and tips for migrating to `loop`


### Conditionals


#### The `when` clause

* <!-- .element: class="fragment" data-fragment-index="0" -->
  The main way to conditionally do something in Ansible is by using the `when` clause
* <!-- .element: class="fragment" data-fragment-index="1" -->
  `when` has identical semantics to a Python _if_ block
   * <!-- .element: class="fragment" data-fragment-index="2" -->
     `when: some_variable is defined`
   * <!-- .element: class="fragment" data-fragment-index="3" -->
     `when: env_name == 'staging'`

<pre class="fragment" data-fragment-index="4"><code data-trim data-noescape>
- name: Some task
  ansible.builtin.command:
    cmd: do something
  <mark>when</mark>: &lt;condition is true&gt;
</code></pre>


##### Exercise: Make our tasks conditional

* Tasks in <!-- .element: class="fragment" data-fragment-index="0" -->`playbook-dict.yml` assume a _staging_ environment
<pre><code data-trim data-noescape>
  vars:
    .
    <mark>env_name: staging</mark>
</code></pre>
* Add a conditional to  the <!-- .element: class="fragment" data-fragment-index="1" -->`lineinfile` tasks
   + Only execute if <!-- .element: class="fragment" data-fragment-index="2" -->`env_name` is *production*
* Re-run the playbook <!-- .element: class="fragment" data-fragment-index="3" -->

<pre  class="fragment" data-fragment-index="4"><code data-trim>
  - name: Add database config to /etc/myapp/config
    ansible.builtin.lineinfile:
    .
    .
    when: env_name == 'production'
</code></pre>


##### Exercise: Override default *env_name* to run tasks

* *env_name* is set to _staging_ by default
* Override this to be _production_ and re-run playbook
* Hint: What type of variable has precedence in this situation?

<pre  class="fragment" data-fragment-index="0" style="font-size:12pt;"><code data-trim data-noescape>
ansible-playbook playbook-dict.yml <mark>-e env_name=production</mark>
</code></pre>



### Protecting Secrets in Ansible


#### Protecting Secrets in Ansible

* <!-- .element: class="fragment" data-fragment-index="0" -->
  There is one big problem with `playbook-dict.yml`
* <!-- .element: class="fragment" data-fragment-index="0" -->
  The database password is in plain text in playbook
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Ansible provides a tool for managing secrets
* <!-- .element: class="fragment" data-fragment-index="3" -->
  `ansible-vault` helps you encrypt/decrypt files containing secrets for your app

<pre class="fragment" data-fragment-index="4" style="font-size:13pt;"><code data-trim>
$ ansible-vault --help

Usage: ansible-vault [create|decrypt|edit|encrypt|encrypt_string|rekey|view] [options] [vaultfile.yml]

encryption/decryption utility for Ansible data files

Options:
--ask-vault-pass      ask for vault password
-h, --help            show this help message and exit
</code></pre>


#### `ansible-vault`

* Uses AES256 encryption
* Secret files can be
   * Loaded automatically as `host_vars` or `group_vars` inventory files
   * Included with `include_vars` or `vars_files` directives
* Encrypted files can be distributed with your application


##### Exercise: Protecting our database passwords
* Create a new file called `secrets.yml`
   ```
   mkdir -p group_vars/web
   vim group_vars/web/secrets.yml
   ```
   ```
   ---
   vault_staging_database_password: <some password>
   vault_production_database_password: <some password>
   ```
   <!-- .element: style="font-size:12pt;"  -->
* Encrypt `group_vars/web/secrets.yml`
   ```
   ansible-vault encrypt group_vars/web/secrets.yml
   New Vault password:
   Confirm New Vault password:
   Encryption successful

   ```
   <!-- .element: style="font-size:12pt;"  -->
   * Make sure you can remember your password!


#### Integrating vaulted secrets
* Replace references to staging/production database passwords
   ```
   database:
     staging:
       password: "{{ vault_staging_database_password }}"
     production:
       password: "{{ vault_production_database_password }}"
   ```


#### Running Ansible with vault
* Run playbook using <code style="color:red;">--ask-vault-pass</code> flag
   <pre style="font-size:13pt;"><code data-trim data-noescape>
    ansible-playbook playbook-dict.yml <mark>--ask-vault-pass</mark>
    Vault Password: ******
   </code></pre>


#### Alternate ways to provide vault password
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Typing the vault password all the time is annoying! 
* <!-- .element: class="fragment" data-fragment-index="1" -->
  You can put your vault password in a file 
   ```
   echo "mysecretpassword" > .vault_password
   ```
* Then run playbook with argument with <!-- .element: class="fragment" data-fragment-index="3" -->`--vault-id`
   <pre style="font-size:11pt;"><code class="shell" data-trim data-noescape>
   ansible-playbook <mark>--vault-id .vault_password</mark> playbook-dict.yml
   </code></pre>
* Be sure you add this file to <!-- .element: class="fragment" data-fragment-index="4" -->`.gitignore`!!!


#### Using a script
* <!-- .element: class="fragment" data-fragment-index="0" -->
  You can also retrieve your vault password using a script
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Integrate a password manager:
  - [titan](https://www.titanpasswordmanager.org/)
  - [gopass](https://github.com/gopasspw/gopass)
  - [kpcli](https://kpcli.sourceforge.io/)
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Example executable script
  ```
  #!/bin/sh
  gopass show prod/ansible_vault_password
  ```
  <!-- .element: class="fragment" data-fragment-index="3" -->
  ```bash
  chmod +x get_vault_pass.sh
  ```
  <!-- .element: class="fragment" data-fragment-index="4" -->
  ```
  ansible-playbook --vault-id get_vault_pass.sh some-playbook.yml
  ```
  <!-- .element: class="fragment" data-fragment-index="5" style="font-size:12pt;" -->


#### Managing multiple vault secrets
* As of Ansible 2.4 it is possible to have multiple encryption keys
* <!-- .element: class="fragment" data-fragment-index="0" -->Change directory
   ```
    cd $INTRO_ANSIBLE_DIR/vault-management
    tree
    ├── ansible.cfg.example
    ├── group_vars
    │   ├── dev
    │   │   └── dev-secret.yml
    │   └── prod
    │       └── prod-secret.yml
    └── inventory
        └── hosts
   ```


#### Using separate vault passwords
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Create separate dev secret and use it to encrypt file
  ```
  echo "mydevvaultpassword" > .dev_vault_password
  ```
  <!-- .element: class="fragment" data-fragment-index="1" -->

  ```
  ansible-vault encrypt --encrypt-vault-id dev \
       --vault-id dev@.dev_vault_password  group_vars/dev/dev-secret.yml
  ```
  <!-- .element: class="fragment" data-fragment-index="2" style="font-size:13pt;"  -->
  <pre class="fragment" data-fragment-index="2" style="font-size:11pt;"><code data-trim data-noescape>
    $ANSIBLE_VAULT;1.2;AES256;<mark>dev</mark>
    66313465646336616231323030633961613464613065373138333862303936333266653366366639
    3965323362353061396662623835636138343534363239390a333332316361343737666137396439
  </code></pre>
* <!-- .element: class="fragment" data-fragment-index="3" -->
  Do the same with the prod secret

  ```
  echo "myprodvaultpassword" > .prod_vault_password
  ```
  <!-- .element: class="fragment" data-fragment-index="4" -->
  ```
  ansible-vault encrypt --encrypt-vault-id prod \
     --vault-id prod@.prod_vault_password  group_vars/prod/prod-secret.yml
  ```
  <!-- .element: class="fragment" data-fragment-index="5" style="font-size:13pt;" -->
  <pre class="fragment" data-fragment-index="6" style="font-size:11pt;"><code data-trim data-noescape>
    $ANSIBLE_VAULT;1.2;AES256;<mark>prod</mark>
    33633335336634393739363636633039376334303533636336373636663139383837663531353134
    6536396633616636383734656439643334653739346462660a323832643834613636393339346232
  </code></pre>


#### Accessing vaulted files
* <!-- .element: class="fragment stretch" data-fragment-index="0" -->
  You can now use vault-ids when accessing vaulted files
   ```
   ansible-vault view --vault-id dev@.dev_vault_password \
      group_vars/dev/dev-secret.yml
   ```
   ```
   ansible-vault view --vault-id prod@.prod_vault_password \
      group_vars/prod/prod-secret.yml
   ```


#### Vault Ids and ansible.cfg
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Of course you can save all this extra typing by configuring vault in
  `ansible.cfg`
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Create `ansible.cfg` in the current directory and add the following
   ```ini
   [defaults]

   vault_identity = dev
   vault_encrypt_identity = dev
   vault_identity_list = dev@.dev_vault_password, prod@.prod_vault_password
   ```
   <!-- .element: style="font-size:11pt;"  -->
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Now you no longer need to use `--vault-id` arguments
  ```
  ansible-view view group_vars/dev/dev-secret.yml
  ```
  ```
  ansible-view view group_vars/prod/prod-secret.yml
  ```


#### Adding secure content inline
##### `ansible-vault encrypt_string`
* <!-- .element: class="fragment" data-fragment-index="0" -->
  It is possible to add secure content in a playbook inline
* <!-- .element: class="fragment" data-fragment-index="1" -->
  `encrypt_string` generates vaulted output that can be added to a playbook
   ```
   echo "mysecretPas2wurd1" | ansible-vault  encrypt_string --stdin-name vault_my_password
   ```
   <!-- .element: style="font-size:10pt;"  -->
   ```
    vault_my_password: !vault |
          $ANSIBLE_VAULT;1.1;AES256;dev
          37326562653730353232346530336334346163633964373732653132370a373
          353439303265373737653963396666653638366639633966666536383666583
   ```
   <!-- .element: style="font-size:12pt;"  class="fragment" data-fragment-index="2" -->
* <!-- .element: class="fragment" data-fragment-index="3" -->
  Discuss pros and cons


#### Summary
* `ansible-vault` is a way to keep secrets safe
   * passwords
   * API keys
   * SSL keys
* Easy to distribute encrypted secrets with code without compromising them
* Automatically integrates into automation tasks


#### Destroy VM
* We are now done with the VM we've been using
* Before we move on we need to stop the current Vagrant VM

```
vagrant halt
vagrant destroy
```



### Orchestrating Applications
#### Handlers


#### Orchestration
* <!-- .element: class="fragment" data-fragment-index="0" -->
  An application may need to interact with multiple services or other app
  components
*  <!-- .element: class="fragment" data-fragment-index="1" -->Ansible provides tools to manage services when deploying or updating
   + <!-- .element: class="fragment" data-fragment-index="2" -->After code is deployed
   + <!-- .element: class="fragment" data-fragment-index="3" -->When a configuration is created or updated
   + <!-- .element: class="fragment" data-fragment-index="4" -->When another service is activated/deactivated


#### Create a Cluster

```bash
cd $INTRO_ANSIBLE_DIR/handlers
vagrant up
```

This sets up a cluster in vagrant consisting of 3 separate hosts<!-- .element: class="fragment" data-fragment-index="0" -->


#### Our Application Cluster <!-- .slide: class="image-slide" -->
![counter app](img/handler-cluster.svg "Counter app")


#### Setup Machines
* <!-- .element: class="fragment" data-fragment-index="0" -->
  The `provision-hosts.yml` playbook performs some basic setup on host
  + <!-- .element: class="fragment" data-fragment-index="1" -->
    update `apt` cache
  + <!-- .element: class="fragment" data-fragment-index="2" -->
    Set up locales
  + <!-- .element: class="fragment" data-fragment-index="3" -->
    Add entries to `/etc/hosts` for resolving hosts
* <!-- .element: class="fragment" data-fragment-index="4" -->
  The `deploy.yml` configures/deploys respective hosts
    + <!-- .element: class="fragment" data-fragment-index="5" -->
      web server running nginx
    + <!-- .element: class="fragment" data-fragment-index="6" -->
      app server running a basic Python Flask application
    + <!-- .element: class="fragment" data-fragment-index="7" -->
      a redis server


#### Run Setup Playbook
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Set up vault password identity
  ```bash
  echo train > .vault_password
  ```
  - Uncomment lines 12-13 in `ansible.cfg`
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Run playbooks
   ```
   ansible-playbook -K provision-hosts.yml deploy.yml
   ```
* <!-- .element: class="fragment" data-fragment-index="2" -->
  You can now view your [website](http://my-counter.testsite:8080)

<!-- .element: class="stretch"  -->


#### Repeating Playbook Runs
* Run the `deploy.yml` playbook a few times
   ```bash
   ansible-playbook deploy.yml
   ```
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Note that many tasks display no change (i.e. <code style="color:green;">ok</code>)
    <asciinema-player  autoplay="0"  loop="loop" font-size="medium" speed="1" theme="solarized-light" src="lib/idempotent-tasks.json" start-at="15" cols="80" rows="10"></asciinema-player>
* <!-- .element: class="fragment" data-fragment-index="1" -->
  A few are always <code style="color:orange;">changed</code>; notably _restart_ tasks


#### Repeating Playbook Runs
* Idempotent behaviour does not apply to certain types of tasks, for example:
  + cache updates <!-- .element: class="fragment" data-fragment-index="0" -->
  + restart services <!-- .element: class="fragment" data-fragment-index="1" -->
* These tasks always display <!-- .element: class="fragment" data-fragment-index="2" --><code style="color:orange;">changed</code>


####  Performing One-off tasks
* Often necessary to trigger certain actions on service when a config is created or changed
   + start
   + restart
   + reload
* Preferable to (re)start services only when necessary
   + config changed
   + application updated


### Handlers
* <!-- .element: class="fragment" data-fragment-index="0" -->
  A _handler_ is a **task** that Ansible will execute only once at the end of a play
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Handlers are triggered using the _notify_ keyword
  <pre style="font-size:13pt;"><code data-trim data-noescape>
    tasks:
      - name: Change some configuration file
        template:
          dest: /etc/some/config
          .
        <mark>notify: restart service</mark>
    <div class="fragment" data-fragment-index="2">
    handlers:
      - name: <mark>restart service</mark>
        systemd:
          name: service
          state: restartd
    </div>
  </code></pre>
* Will only execute if task result is <!-- .element: class="fragment" data-fragment-index="3" --><code style="color:orange;">changed</code>
* Execute only once <!-- .element: class="fragment" data-fragment-index="4" -->


#### Using Handlers
* `basic-handler.yml` simulates running basic tasks on our servers
   ```
   ansible-playbook basic-handler.yml
   ```
* Debug tasks do not result in any changes
* Each play has a task to restart specific services
* Let's modify this to run these as _handlers_


#### Create First Handler
* Create a section called _handler_ and move the _restart nginx_ task into it
   ```
     handlers:
       - name: restart nginx
         systemd:
            name: nginx
            state: restarted
   ```


#### Notifying Handlers

* Set tasks to notify handler when changed
   <pre style="font-size=13pt;"><code data-trim data-noescape>
    - name: Set up nginx task 1
      <mark>notify: restart nginx</mark>
      .
      .
    - name: Set up nginx task 2
      <mark>notify: restart nginx</mark>
      .
      .
    - name: Set up nginx task 3
      <mark>notify: restart nginx</mark>
      .
   </code></pre>
* Re run the playbook
* This time the _restart nginx_ task does not run


#### Triggering a Handler
* Tasks in <!-- .element: class="fragment" data-fragment-index="0" -->`basic-handler.yml` have a special attribute to make Ansible think
  a change has occurred
  <pre><code data-trim data-noescape>
  - name: Set up nginx task 1
    .
    <mark>changed_when: nginx_config_changed | default(false)</mark>
  </code></pre>
* We can pass extra variables to interpret task as changed <!-- .element: class="fragment" data-fragment-index="1" -->
   ```
   ansible-playbook basic-handler.yml -e nginx_config_changed=true
   ```
   <!-- .element: style="font-size:13pt;"  -->


#### Triggering Handlers
* _notify_ trigger for a handler can be called multiple times in a play
* Regardless, handler will only be fired off once per play
* Try the preceding exercise again with multiple extra vars
* Handler should only run once


#### Back to our Application
* Let's modify the `deploy.yml` playbook to use handlers


#### The Nginx Play
<pre style="font-size:13pt;"><code data-trim data-noescape>
  tasks:
    - name: Install nginx
      .
      .
    - name: Add nginx config
      .
      <mark>notify: restart nginx</mark>

    - name: Symlink nginx conf to activate
      .
      <mark>notify: restart nginx</mark>

  handlers:
    - name: restart nginx
      service:
        name: nginx
        state: restarted
</code></pre>


#### The Redis Play
<pre style="font-size:13pt;"><code data-trim data-noescape>
  tasks:
    - name: Install redis
      .
      .
    - name: Enable redis on boot
      .
    - name: Set bind address to allow requests from other machines
      .
      <mark>notify: start redis</mark>
    - name: Modify redis config to work as a cache
      .
      <mark>notify: start redis</mark>

  handlers:
    - name: start redis
      systemd:
        name: redis-server
        state: restarted
</code></pre>


#### Application Setup Play
<pre style="font-size:11pt;"><code data-trim data-noescape>
  tasks:
      .
    - name: Checkout application from git
      .
      <mark>notify: restart gunicorn</mark>
    - name: Install python libraries
      .
      <mark>notify: restart gunicorn</mark>

    - name: Template in configuration
      .
      <mark>notify: reload gunicorn</mark>

    - name: Add systemd config
      .
      <mark>notify: restart gunicorn</mark>

  handlers:
    - name: reload gunicorn
      systemd:
        name: gunicorn
        state: reloaded

    - name: restart gunicorn
      systemd:
        name: gunicorn
        state: restarted
</code></pre>

* <!-- .element: class="fragment" data-fragment-index="0" -->
  Now run the `deploy.yml` play with handlers defined


#### Update our application
* The Python application is checked out to <!-- .element: class="fragment" data-fragment-index="0" -->_v1_ by default
* Let's update it to <!-- .element: class="fragment" data-fragment-index="1" -->_v2_ by passing an extra var to the playbook
   ```
   ansible-playbook deploy.yml -e app_version=v2
   ```
* This should trigger the <!-- .element: class="fragment" data-fragment-index="2" -->_restart gunicorn_ handler
* Other handlers should not run <!-- .element: class="fragment" data-fragment-index="3" -->
   + `restart nginx`
   + `restart redis`
   + `reload gunicorn`


#### Modifying Nginx Service
* Let's make some changes to the nginx service
* Create a directory where we can add an SSL cert

```
    - name: Create directory for ssl certs
      file:
        path: /etc/nginx/ssl
        state: directory
        owner: root
        group: root
        mode: '0755'
      notify: restart nginx
```


##### Set up application for SSL
* Copy and template in self-signed certificate and key
  ```
     - name: Add ssl cert for site
      copy:
          dest: /etc/nginx/ssl/site.crt
          src: files/site.crt
          owner: root
          group: root
          mode: '0644'
      notify: restart nginx

    - name: Add ssl key for site
      copy:
          dest: /etc/nginx/ssl/site.key
          content: "{{ nginx_key }}"
          owner: root
          group: root
          mode: '0600'
      notify: restart nginx
  ```
   <!-- .element: style="font-size:10pt;"  -->


##### Modify Nginx Config
* Change nginx config to use SSL certificate
   ```
   server {
     listen 80;
     server_name {{ domain_name }};

     # Redirect all HTTP requests to HTTPS with a 301 Moved Permanently response.
     return  301         https://{{ domain_name }}:8443;

   }
   server {
    listen 443 ssl;
    server_name  {{ domain_name }};
    ssl_certificate /etc/nginx/ssl/site.crt;
    ssl_certificate_key /etc/nginx/ssl/site.key;

    location / {
      include proxy_params;
      proxy_pass http://{{ hostvars[groups.app[0]].ansible_default_ipv4.address }}:5000;
    }
   }
   ```
  <!-- .element: style="font-size:12pt;"  -->
* Run the deploy playbook again
* View [website](https://my-counter.testsite:8443) on port 8443


#### Summary
* Handlers are an important way to _orchestrate_ parts of application
* Ensure that services are only restarted when they need to be



### Ansible Best Practices


#### Naming plays and tasks
* Always use **`name:`** attribute in _plays_ and _tasks_
  - Explains to others what you're doing
  - Makes it easier to find tasks when they fail
    ```yaml
    - name: Copy file to host
      copy:
        ...
    ```


#### Managing vault secrets
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Projects can have many _vault_-encrypted files
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Vaulted variables impossible to find (i.e. using `grep`)
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Recommend: In your secrets file(s) use **`vault_`** prefix for variables
  ```bash
  ansible-vault edit group_vars/mygroup/vault-secrets.yml
  ```
  ```
  ---
  vault_database_password: 1password!
  ```
* <!-- .element: class="fragment" data-fragment-index="3" -->
  Reference that variable in same group directory
  ```bash
  vim group_vars/mygroup/vars.yml
  ```
  ```
  ---
  database_password: "{{ vault_database_password }}"
  ```


#### Managing secrets inline
* Alternative approach to storing vault passwords is to generate with `encrypt_string`

  ```
  ansible-vault encrypt_string -n database_password 1password! >> group_vars/mygroup.yml
  ```
  <!-- .element: style="font-size:11pt;" class="fragment" data-fragment-index="0"  -->
  ```
  ---
  database_password: !vault |
          $ANSIBLE_VAULT;1.1;AES256
          63643630383337363839333664313932373032643432353331623364383836663162336439646336
          3262396136313331636265626131353065363235343631650a323763313766306333633763333030
          63333037383837623665323764636439346563376334636232383531346338343038643062316135
          3931643462356363310a313562313738313132643032326464303565666164366264373531313963
          3563
  ```
  <!-- .element: style="font-size:11pt;" class="fragment" data-fragment-index="1"  -->
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Doesn't matter if file is a group directory or file with name of group
* <!-- .element: class="fragment" data-fragment-index="3" -->
  Easy to find in code, but difficult to see content later

<!-- .element: class="stretch"  -->


#### Bad practice: relying on `command`-family modules
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Beginners often rely heavily on _command_ family modules (i.e `command`, `shell`
  and `raw`)
  ```
  - name: Checkout code
    cmd: git checkout https://github.com/somerepo/somecode.git

  - name: Create subdirectory
    shell: >
      cd somecode && mkdir dist

  - name: Build code
    shell: >
      cd somecode && npm install
  ```
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Very difficult to detect _changed_ state
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Approach is not _idempotent_


#### Use modules
* <!-- .element: class="fragment" data-fragment-index="0" -->
  _command_, _shell_, etc. should be last resort
* <!-- .element: class="fragment" data-fragment-index="1" -->
  _Always_ use modules in first instance
  ```
  - name: Checkout code
    git:
      repo: https://github.com/somerepo/somecode.git

  - name: Create subdirectory
    file:
      path: somecode/dist
      owner: "{{ ansible_user }}"
      group: "{{ ansible_user }}"
      mode: 0644
      state: present

  - name: Build code
    npm:
      path: somecode
  ```

<!-- .element: class="stretch"  -->


#### Specific modules vs cmd
* <!-- .element: class="fragment" data-fragment-index="0" -->
  How do I find the module that I need?
  ```bash
  ansible-doc -l
  ```
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Is there ever a reason to use `cmd`?
  - <!-- .element: class="fragment" data-fragment-index="2" -->
    There are cases where no available module does what you need
  - <!-- .element: class="fragment" data-fragment-index="3" -->
    Existing module doesn't cover functionality
  - <!-- .element: class="fragment" data-fragment-index="4" -->
    We will cover this in day 2 session


#### Linting Ansible
##### `ansible-lint`
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Best way to develop good habits
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Warn about deprecated features
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Help make code more readable
* <!-- .element: class="fragment" data-fragment-index="3" -->
  Can be integrated in dev workflow
  - git commit hooks
  - in editor
<!-- .element: class="stretch"  -->


##### Exercise: Fix playbook
* Fix linting errors in `sample-playbook.yml`
  ```bash
  cd $INTRO_ANSIBLE_DIR/linting
  ansible-lint sample-playbook.yml
  ```


### Wrap up
* Stop vagrant from handlers lesson
  ```
  cd $INTRO_ANSIBLE_DIR/handlers
  vagrant halt && vagrant destroy
  ```



### Organising Infrastructure Code
#### Roles and Collections


#### Roles

```
$ cd $INTRO_ANSIBLE_DIR/ansible-roles
.
├── hosts
└── project.yml
```


#### Making tasks reusable

* At some point in your development process, you may come across
  bits that will be useful across multiple projects
* Important to follow _DRY_ (don't repeat yourself) principles in infrastructure code


#### Identify reusable components
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Have a look at `project.yml`
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Tasks simulate steps involved in setting up an application
   * Installing language libraries
   * Deploying configuration
   * Set up DB
   * Run handlers to start services
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Components could be reused in other projects


#### Roles

* A mechanism for reusing code in Ansible
  - within a project
  - accross multiple projects
* Typically designed for a specific purpose
* Not executable on their own


#### Decomposing playbook into roles
* Let's start decomposing the playbook into reusable components starting with
  app setup


#### Creating a basic role
* <!-- .element: class="fragment " data-fragment-index="0" -->
  Run `ansible-galaxy init`
  ```
  ansible-galaxy init --init-path roles setup-app
  ```
* <!-- .element: class="fragment stretch" data-fragment-index="1" -->
  This creates basic skeleton for a role
   <pre><code data-noescape data-trim>
tree roles
.
├── hosts
├── project.yml
└── <mark>roles
        └── setup-app
            ├── defaults
            │   └── main.yml
            ├── files
            ├── handlers
            │   └── main.yml
            ├── meta
            │   └── main.yml</mark>
</code></pre>

<!-- .element: class="stretch"  -->


#### Structure of a role
  <pre><code data-trim data-noescape>
  ./roles              <span class="fragment" data-fragment-index="0"><-- base directory depends on config</span>
    └── role-name     <span class="fragment" data-fragment-index="1"><-- Arbitrary; what you will import in "roles:"</span>
        ├── defaults
        │   └── main.yml
        ├── files
        │   └── someconfig.conf
        ├── handlers
        │   └── main.yml
        ├── meta
        │   └── main.yml
        ├── tasks
        │   └── main.yml
        ├── templates
        │   └── sometemplate.j2
        └── vars
            └── main.yml
  </code></pre>
<!-- .element: style="font-size:13pt;"  -->
  * Each of these files/folders is optional


#### Components of a role

* tasks
  - tasks that the role will perform
* files
  - Files that will be uploaded
* templates
  - Jinja2 templates that the role will use
* handlers
  - Handlers that will be called from tasks


#### Components of a role (continued)

* vars
  - Variables needed by role (shouldn't be overridden)
* defaults
  - Variables that can be overridden
* meta
  - Dependency information


#### File and directory naming conventions

* The naming of components correspond to directories in the role
* Ansible will look in these directories automatically when running a role
* YAML files named `main.yml` will be loaded automatically when role is
  executed
* Nearly all components are optional


#### Typical Use Cases
* Install supporting libraries/software to multiple machines
* Standardise provisioning of machines across vendors
   - AWS
   - Azure
   - OpenStack
* Tasks needed across entire infrastructure
   - Security hardening


#### Implementing roles
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Move following tasks from `project.yml` to `roles/setup-app/tasks/main.yml`
  ```
  - name: Update apt cache
    .
  - name: Check out code for project
    .
  - name: Create python virtual environment
    .
  ```


#### Implementing roles
* <!-- .element: class="fragment" data-fragment-index="0" -->Open the handlers main file
   ```
   $EDITOR roles/setup-app/handlers/main.yml
   ```
* <!-- .element: class="fragment" data-fragment-index="1" -->
  From `project.yml`, remove _app_ handler and put it into `handlers/main.yml`
   ```
   - name: restart app
   ```
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Run the `project.yml` playbook again. Note missing *setup-app* tasks


#### Integrating role into play
##### The `roles` keyword
* To integrate a role into a project you need to add a new section to your
  play called `roles`
  <pre class="fragment" data-fragment-index="0"><code data-trim data-noescape>
  - name: Set up python application
    hosts: localhost
    vars:
      .
      .
    <mark>roles:</mark>
    <mark class="fragment" data-fragment-index="1">  - role: setup-app</mark>
  </code></pre>
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Run `project.yml` again. Note the *setup-app* tasks and handler both run with a new label
  <pre><code data-trim data-noescape>
  TASK [<mark>setup-app :</mark> Update apt cache]
  </code></pre>


#### Where do roles live?
* In order of _decreasing_ precedence
  - <!-- .element: class="fragment" data-fragment-index="0" -->
    Custom location configured in `ansible.cfg`
     ```ini
     [defaults]
     roles_path = ~/ansible_roles
     ```
  - <!-- .element: class="fragment" data-fragment-index="1" -->
    In `roles` subdirectory in the same place your playbooks live
     ```
     ansible/
        \
         --- playbook1.yml
         |
         --- roles/
     ```
  - <!-- .element: class="fragment" data-fragment-index="2" -->
    `$HOME/.ansible/roles`
  - <!-- .element: class="fragment" data-fragment-index="3" -->
    In `/etc/ansible/roles` directory


#### Exercise: Continue Refactoring `project.yml` into roles
* As in previous example, break `project.yml` into roles for
  - Configuring app
  - Setting up DB
* See if we can break them up into some useful roles


#### Pre and post tasks

* We still need to make sure that the apt modules runs before
  anything else happens
* Changing these into a *pre_task* ensures it will run before the roles do

<pre class="fragment" data-fragment-index="0"><code data-trim data-noescape>
  <mark>pre_tasks:</mark>

    - name: Update apt cache
      become: yes
      apt:
        update_cache: yes
</code></pre>


#### Open source roles

[Ansible Galaxy](https://galaxy.ansible.com)

* A repository of ansible roles
* Thousands of opensource roles for any purpose
* Can be easily imported into your projects


#### Summary

* Roles provide useful way to reuse code accross projects
  - Simple to include
* Designed to facilitate automation
  - Directory structure
  - Naming conventions
* Ansible Galaxy is an Open Source repository of roles available for all
  purposes



### Distributing and importing roles


#### Distributing and importing roles
```
cd $INTRO_ANSIBLE_DIR/ansible-roles-pt2
```
* Roles make it easy to distribute/reuse Ansible tasks
* Distribute via conventional source code management tools
* Open Source your Ansible


#### Importing roles
* <!-- .element: class="fragment" data-fragment-index="0" -->
  The `ansible-galaxy` command line tool can be used to import roles from
  different sources
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Install a role from Ansible Galaxy 
   ```bash
   ansible-galaxy install  role-name
   ```
   <!-- .element: style="font-size:10pt;"  -->
* <!-- .element: class="fragment" data-fragment-index="2" -->
  or install version 1.0.0 of a role from a repository on GitHub 
   ```
   ansible-galaxy install git+https://github.com/user/role-name.git,1.0.0
   ```
   <!-- .element: style="font-size:10pt;"  -->
* Use the role in your playbook <!-- .element: class="fragment" data-fragment-index="3" -->
   ```
   role:
     - role: role-name
   ```
   <!-- .element: style="font-size:10pt;"  -->

<!-- .element: class="stretch"  -->


##### Exercise: Install a role
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Go to the [Ansible Galaxy Website](https://galaxy.ansible.com)
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Find a role that interests you
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Install it using `ansible-galaxy install`
* <!-- .element: class="fragment" data-fragment-index="3" -->
  Example
   ```bash
   ansible-galaxy install geerlingguy.java
   ```


#### Where does Ansible install roles?
* <!-- .element: class="fragment" data-fragment-index="0" -->
  See *Introduction to Ansible* lesson
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Depends on configuration
* <!-- .element: class="fragment" data-fragment-index="2" -->
  By default roles are installed in
   ```bash
   $HOME/.ansible/roles
   ```
* <!-- .element: class="fragment" data-fragment-index="3" -->
  Can be overridden using **<code>roles_path</code>** in `ansible.cfg`
   ```ini
   [defaults]
   roles_path = <path>
   ```
   - note rules about precedence from previous lesson


##### Exercise: Change role install location
* Install role into 
   ```
   $INTRO_ANSIBLE_DIR/ansible-roles/roles
   ```
* <!-- .element: class="fragment" data-fragment-index="0" -->
  First create `ansible.cfg` in the current directory
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Specify `roles_path`
   ```ini
   [defaults]
   roles_path = roles
   ```
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Re-run `ansible-galaxy install <role>`


#### Distributing your Roles
* Ansible galaxy makes it easy to import roles
   - don't reinvent the wheel if you don't have to
* What if you want to distribute your own roles?


##### Exercise: Distributing a Role
* The `ansible-roles` directory contains a very basic role
   ```
    .
    └── my_role
        ├── defaults
        │   └── main.yml
        ├── files
        ├── handlers
        │   └── main.yml
        ├── meta
        │   └── main.yml
        ├── README.md
   ```
* Let's make it ready for distribution


#### Making a Role *distributable*
* Need to fill in <!-- .element: class="fragment" data-fragment-index="0" -->*meta* information about dependencies
* <!-- .element: class="fragment" data-fragment-index="1" -->Open file `meta/main.yml` in your editor
   <pre><code data-trim data-noescape>
└── my_role
<mark>  ├── meta
    │  └── main.yml</mark>
    └── tasks
        └── main.yml</code></pre>
* <!-- .element: class="fragment" data-fragment-index="2" -->
  The `ansible-galaxy init` tool has already set up most of what you might
  need


##### Setting up dependencies
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Dependencies tell `ansible-galaxy` to pull in other related roles
   ```
   # meta/main.yml
   dependencies:
     - role: antonchernik.nodejs
     - role: my-other-role
       vars: 
         someattribute: green
   ```
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Must be defined, even if you have no dependencies 
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Perfectly normal to have an empty list
   ```
   # meta/main.yml
   dependencies: []
   ```


#### Distributing your Role
* Once you have defined dependencies simply upload to your SCM of choice
* Assume you have pushed to http://github.com/myaccount/my_role.git repo
  ```
  ansible-galaxy install git+https://github.com/myaccount/my_role.git
  ```
  <!-- .element: style="font-size:13pt;"  -->
* <!-- .element: class="fragment" data-fragment-index="0" -->
   Later we'll see how to push a role to [ansible-galaxy](https://galaxy.ansible.com)


##### Exercise: Install a role from GitHub
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Install random [ansible-nodejs](https://github.com/heytrav/ansible-nodejs) role
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Note that you need to specify `git+<url>`

```
ansible-galaxy install git+https://github.com/heytrav/ansible-nodejs.git
```
<!-- .element: class="fragment" data-fragment-index="2" style="font-size:14pt;"-->


#### Managing roles in a project
* Your project might depend on a number of roles
* Adding/tracking each manually can get tedious
* Use a *requirements.yml* file


#### Role Requirements File
* A YAML file (surprise)
* Specify a list of requirements with URL, version, and local name
   ```
    - src: git@github.com/my-account/my_role.git
      scm: git
      version: master
      name: my_role

    - src: some-other-role
      version: 1.0
   ```
* Place this file in your project somewhere 


#### Install roles using a requirements file
* Use `ansible-galaxy` command line tool as before
   ```
   ansible-galaxy install -r requirements.yml
   ```
* Wash, rinse, repeat


#### Summary
* Ansible roles useful way to distribute reusable tasks
* Combined with requirements file, very useful way to manage infrastructure
  dependencies


#### Collections Primer
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Released Sept 2020
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Major reorganisation of `ansible/ansible` project
  - <!-- .element: class="fragment" data-fragment-index="2" -->
    Many plugins/modules moved to smaller repositories
  - <!-- .element: class="fragment" data-fragment-index="3" -->
    Community and vendor sub-projects have better control
* <!-- .element: class="fragment" data-fragment-index="4" -->
  `ansible-base` now just contains
  - core Ansible programs (eg. -playbook, -galaxy)
  - docs
  - subset of modules
* <!-- .element: class="fragment" data-fragment-index="5" -->
  The rest has been moved into _collections_


#### What are collections?
* <!-- .element: class="fragment" data-fragment-index="0" -->
  A packaging format for bundling and distributing Ansible content
  - plugins
  - roles
  - modules
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Can be released independently of `ansible-base`
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Own source control


#### Collection nomenclature
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Fully Qualified Collection Name (FQCN) is the way to reference a specific
  module, role or plugin
* <!-- .element: class="fragment" data-fragment-index="1" -->
  FQCN takes form
  ```bash
  namespace.collection.content_name
  ```
* <!-- .element: class="fragment" data-fragment-index="2" -->
  _Namespace_ is top level organisation and reserved for owners of project


#### Excercise
Explore available collections and modules using `ansible-doc`

```
ansible-doc -l
```


#### Collection examples

| Ansible &le; 2.9 modules |  Ansible &ge; 2.10 modules | Description |
| ---  | --- | --- |
| `file` | `ansible.builtin.file` | Set attributes of files |
| `os_server` | `openstack.cloud.server` | Manage a server in OpenStack |
| `openssl_certificate` | `community.crypto.openssl_certificate` | Generate or check openssl cert |
| `mysql_db`  | `community.mysql.mysql_db` | Add or remove a mysql database from a remote host |

<!-- .element: style="font-size:13pt;" class="fragment" data-fragment-index="0"  -->
* <!-- .element: class="fragment" data-fragment-index="1" -->
`ansible.builtin` maintained by Ansible directly
* <!-- .element: class="fragment" data-fragment-index="2" -->
`openstack.` top level for projects owned by Openstack project
* <!-- .element: class="fragment" data-fragment-index="3" -->
`community.` top level for community contributions


#### Using collections in tasks
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Ansible &le;2.9 way of writing tasks
  ```yaml
  - name: Create a file
    file:
      path: /path/to/myfile.txt
      state: touch
      owner: ubuntu
  ```
  <!-- .element: style="font-size:11pt;"  -->
  ```yaml
  - name: Create a certificate signing request
    openssl_csr:
      path: /etc/ssl/csr/www.ansible.com.csr
      privatekey_path: /etc/ssl/private/ansible.com.pem
      force: yes
      common_name: www.ansible.com
  ```
  <!-- .element: style="font-size:11pt;"  -->
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Same thing &ge; 2.10 using FQCNs for modules
  ```yaml
  - name: Create a file
    ansible.builtin.file:
      path: /path/to/myfile.txt
  ```
  <!-- .element: style="font-size:11pt;"  -->
  ```yaml
  - name: Create a certificate signing request
    community.crypto.openssl_csr:
      path: /etc/ssl/csr/www.ansible.com.csr
      privatekey_path: /etc/ssl/private/ansible.com.pem
      force: yes
      common_name: www.ansible.com
  ```
  <!-- .element: style="font-size:11pt;"  -->

<!-- .element: class="stretch"  -->


#### Backwards compatability
* <!-- .element: class="fragment" data-fragment-index="0" -->
  No need to change all of your code
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Existing playbooks, etc. should work same as they have before
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Ansible will automatically resolve module name to the new collection
* <!-- .element: class="fragment" data-fragment-index="3" -->
  Using FQCNs is _recommended_
  - Avoid possible ambiguity with other sub-projects in the future
  ```
    cloudscale_ch.cloud.server
    community.hrobot.server 
    openstack.cloud.server
  ```


#### Upgrading Ansible
* <!-- .element: class="fragment" data-fragment-index="0" -->
  If you are still using Ansible &le; 2.9 you need to be careful how you
  upgrade
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Standard pip upgrade will not work
  - repurposing of the `ansible/ansible` package for core only
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Upgrading from 2.9 and below, when using pip you must first remove Ansible and re-install
  ```bash
  pip uninstall ansible ansible-base
  ```
  ```bash
  pip install ansible
  ```


#### Finding modules, plugins, etc.
* Easiest way to find new names is to look in documentation
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Can find FQCN for modules using `ansible-doc`
  ```bash
  ansible-doc -l
  ```
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Same for lookup plugins
  ```bash
  ansible-doc -t lookup -l
  ```
* <!-- .element: class="fragment" data-fragment-index="2" -->
  And inventory plugins
  ```bash
  ansible-doc -t inventory -l
  ```


#### Exercise: convert playbook
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Run handler exercise playbooks with verbose output
  ```
  cd $INTRO_ANSIBLE_DIR/handlers
  ansible-playbook -K provision-hosts.yml deploy -vv
  ```
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Note output about how Ansible resolves modules
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Update playbooks to use FQCN syntax
* <!-- .element: class="fragment" data-fragment-index="3" -->
  Re-run playbooks to see how they behave


#### Online information
* [Google groups announcement](https://groups.google.com/g/ansible-project/c/yFnMgbjqYnU)
* [Collections overview](https://github.com/ansible-collections/overview)
* [Lists of collections](https://docs.ansible.com/ansible/latest/collections/index.html#list-of-collections)
* Search collection namespace on [Ansible Galaxy](https://galaxy.ansible.com)


#### Summary
* Ansible 2.10 brought some major changes to organistion of project
  - collections
* Move towards namespaced modules, plugins etc.
* Split into community and vendor owned subprojects
* Should not break any existing code



## Task conditions


### Task conditions

```console
cd $INTRO_ANSIBLE_DIR/task-conditions
```

```
vagrant up
```


#### Managing task conditions

* Most modules have their own way of defining failure or change of state <!-- .element: class="fragment" data-fragment-index="0" -->
* Some improvise based on return code <!-- .element: class="fragment" data-fragment-index="1" -->
  - `command` family of modules: (`command`, `shell`, `script`, `raw`)
* Shell commands typically return uninformative error codes <!-- .element: class="fragment" data-fragment-index="2" -->
  - 0 for success
  - &ge; 1 for failure
* May be necessary to override Ansible behaviour <!-- .element: class="fragment" data-fragment-index="3" -->

<!-- .element: class="stretch"  -->


#### Managing errors

##### `runtools.yml`

* Run playbook `runtools.yml`
<asciinema-player loop="1" cols="90" theme="solarized-light" start-at="13.0" rows="10" autoplay="1" font-size="medium" src="asciinema/error-playbook.cast"></asciinema-player>
* This playbook tries to:
  - Delete a _non-existent_ branch from the repository
  - Run a script which creates a directory


#### Command tasks and trivial errors
* Deleting non-existent branch causes git to return error
  * i.e. return code not equal `0`
* `command` picks up error and returns failure
* Ideally not something that should fail a play
  * idempotent behaviour
* Want to tell Ansible to _ignore_ these errors and move on


#### Ignore errors

##### `ignore_errors`

* Tell Ansible to continue execution when an error happens
* Accepts a boolean: `true`, `yes`
<pre><code  data-trim data-noescape>
    - name: Delete a branch from repository
      ansible.builtin.command: do_something_that_fails.sh
      <mark class="fragment" data-fragment-index="0">ignore_errors: true</mark>
</code></pre>


#### Exercise: Ignore errors from a task

* In `runtools.yml`
* First task fails to delete a branch that does not exist
* Not a critical error for our playbook
* Alter the first task in `runtools.yml` so errors are ignored
<pre class="fragment" data-fragment-index="0"><code data-trim data-noescape>
    - name: Delete a branch from repository
      ansible.builtin.command: git branch -D notabranch
      args:
        chdir: "{{ repository_destination }}"
      <mark>ignore_errors: true</mark>
</code></pre>


#### Problems with `ignore_errors`

* Blunt way of bypassing errors <!-- .element: class="fragment" data-fragment-index="0" -->
* Output of task can be confusing because it looks like something bad happened <!-- .element: class="fragment" data-fragment-index="1" -->
* May miss failures that are important <!-- .element: class="fragment" data-fragment-index="2" -->
* Shell command output may not be failure <!-- .element: class="fragment" data-fragment-index="3" -->


#### Controlling failure

* Run `runtools.yml` a couple times
* The second task in runs a script `tools.sh` that: 
    * creates a directory
    * fails if directory already exists
* Actually failing because of idempotent behaviour


#### Preventing failure

##### How do we keep playbook from failing?

* One option would be to check if the directory exists before running script
<pre><code data-trim data-noescape>
    - name: Get status of testdir
      ansible.builtin.stat:
        path: "{{ ansible_env.HOME }}/testdir"
      register: stat_output

    - name: Run tools command in working directory
      ansible.builtin.shell: "{{ ansible_env.HOME }}/tools.sh"
      <mark>when: not stat_output.stat.exists</mark>
</code></pre>
* This works, but adds extra unneeded tasks <!-- .element: class="fragment" data-fragment-index="0" -->


#### Defining failed state

##### `failed_when`

* Define when ansible should interpret a task has failed <!-- .element: class="fragment" data-fragment-index="0" -->
* Semantically similar to<!-- .element: class="fragment" data-fragment-index="1" --> _when_ (i.e. conditional)
* Use with<!-- .element: class="fragment" data-fragment-index="2" --> _register_ to capture stdout/stderr
<pre class="fragment" data-fragment-index="3"><code data-trim data-noescape>
    - name: Run some command
      ansible.builtin.shell: do_something_with_error_code.sh
<mark  class="fragment" data-fragment-index="4">    register: my_output
      failed_when: 
      - my_output.rc != 0
      - not my_output.stderr is search('not a fail!')</mark>
</code></pre>


#### Exercise: Do not fail if directory exists

* Use `register` and `failed_when` to keep task from failing if directory exits
<pre  class="fragment" data-fragment-index="0"><code data-trim>
    - name: Run tools command in working directory
      ansible.builtin.shell: "{{ ansible_env.HOME }}/tools.sh"
      register: tools_output
      failed_when: 
        - tools_output.rc != 0
        - not tools_output.stderr is search('already exists')
</code></pre>
* Run playbook again <!-- .element: class="fragment" data-fragment-index="1" -->


#### Defining _changed_ state

* The `runtools.yml` playbook still has a problem
* It always reports the task for running the script as _changed_


#### Defining _changed_ state

##### `changed_when`

* <!-- .element: class="fragment" data-fragment-index="0" -->`changed_when` attribute can be used to define criteria for _changed_
* Also similar semantics to<!-- .element: class="fragment" data-fragment-index="1" --> _when_ conditional 
<pre class="fragment" data-fragment-index="2"><code data-trim data-noescape>
  - name: Perform a task
    ansible.builtin.command: change_something.sh
    changed_when: &lt;condition true&gt;
</code></pre>


##### Exercise: Control task changed state

* Change task in `runtools.yml` to show changed when directory created

<pre class="fragment" data-fragment-index="0"><code data-trim>
    - name: Run tools command in working directory
      ansible.builtin.shell: "{{ ansible_env.HOME }}/tools.sh"
      args:
        chdir: "{{ ansible_env.HOME }}"
      register: tools_output
      failed_when: 
        - tools_output.rc != 0
        - not tools_output.stderr is search('already exists')
      changed_when:
        - tools_output.rc == 0
        - tools_output.stdout is search('Created testdir')
</code></pre>


#### The `command` module  
###### _creates_ and _removes_ 

*  _command_, _script_ and _shell_ all take a special arguments to influence
   behaviour
   - creates
   - removes
* These commands will check ahead of time whether or not a file/directory exists


#### Examples of _creates_ and _removes_

* Task used to install reveal presentation on training machines

```
  - name: Set up reveal presentation
    ansible.builtin.command: npm install
    args:
      chdir: "{{ ansible_env.HOME }}/intermediate-ansible/slides"
      creates: node_modules

  - name: Remove some extra files
    ansible.builtin.command: rm -fr somedirectory
    args:
      removes: somedirectory

```


##### Exercise: modify task to use _creates_ argument

* Can replace `changed_when` clause

<pre  class="fragment" data-fragment-index="0"><code data-trim data-noescape>
    - name: Run tools command in working directory
      ansible.builtin.shell: "{{ ansible_env.HOME }}/tools.sh"
      args:
        <mark>creates: "{{ ansible_env.HOME }}/testdir"</mark>
      register: tools_output
      failed_when: 
        - tools_output.rc != 0
        - not tools_output.stderr is search('already exists')
</code></pre>


##### Exercise: Add task to remove a directory

* Remove the `temporarydir` in task-conditions folder
* Use _removes_ argument

<pre  class="fragment" data-fragment-index="0" style="font-size:11pt;"><code data-trim data-noescape>
    - name: Directory created by script
      ansible.builtin.command: rm -fr {{ ansible_env.HOME }}/temporarydir
      args:
        removes: "{{ ansible_env.HOME }}/temporarydir"
</code></pre>


#### Applied control of task conditions

* `db.yml` creates a db table and adds some images
* There are a couple problems:
  - _Create table for pics_ task always displays changed
  - _Add images to new table_ will fail after first run due to unique constraint
* So let's apply **`changed_when`** and **`failed_when`** to fix this


#### Exercise: Allow task to pass on duplicate errors

* Use _register_ to capture stdout/stderr
* Do not fail if error response has the word "duplicate"

<pre class="fragment" data-fragment-index="0" style="font-size:10pt;"><code data-noescape data-trim >
    - name: Add images to new table
      ansible.builtin.command: >
        psql -U {{ database_user }} -h {{ database_host }} 
        -c "INSERT INTO images (image) VALUES ('{{ item }}')" {{ database }}
      loop: "{{ images }}"
      <mark class="fragment" data-fragment-index="1">register: db_insert</mark>
      <mark class="fragment" data-fragment-index="2">failed_when: 
        - db_insert.rc != 0 
        - not db_insert.stderr is search('duplicate')</mark>

</code></pre>


#### Exercise: only show changed when table created

* First task doesn't fail because of <!-- .element: class="fragment" data-fragment-index="0" -->_if not exists_ clause
* If a table exists postgres outputs <!-- .element: class="fragment" data-fragment-index="1" --> _already exists, skipping_ to stderr

<pre  class="fragment" data-fragment-index="2" style="font-size:10pt;"><code data-trim data-noescape>
    - name: Create table for pics 
      ansible.builtin.command: >
        psql -U {{ database_user }} -h {{ database_host }} 
        -c "CREATE TABLE IF NOT EXISTS images (id SERIAL primary key not null,
        image char(200) not null unique)" {{ database }}
<mark  class="fragment" data-fragment-index="3">      register: create_table_output
      changed_when:
        - create_table_output.rc == 0
        - not create_table_output.stderr is search('already exists')</mark>

</code></pre>


#### Blocks

* Blocks allow for logical grouping of tasks <!-- .element: class="fragment" data-fragment-index="0" -->
* Directives that can be applied to a single task can be applied to groups of
  <!-- .element: class="fragment" data-fragment-index="1" -->
  tasks in a block
  - when clauses
  - privilege escalation


#### Using blocks

  <pre style="font-size:15pt;"><code data-trim data-noescape>
  become: false
  tasks:
    - debug:
        msg: Outside a block

    - name: Set up server
      block:
        <mark>- name: Install nginx
          apt:
            name: nginx
            state: present</mark>
      when:
       - ansible_distribution == "Ubuntu"
      become: true
  </code></pre>

* Run the playbook `block.yml`


#### Recovering from errors

* Sometimes necessary to recover from errors <!-- .element: class="fragment" data-fragment-index="0" -->
* May need to perform tasks to clean up <!-- .element: class="fragment" data-fragment-index="1" -->
* The method in Ansible is to use<!-- .element:parent: class="fragment" data-fragment-index="2" --> _blocks_ 
* blocks used with<!-- .element: class="fragment" data-fragment-index="4" --> _rescue/always_ section similar to _try/catch blocks_ in programming 


#### Error handling with blocks

<pre style="font-size:15pt;"><code data-trim data-noescape>
    - name: Perform an operation that fails
      block:
        - name: This task fails
          ansible.builtin.command: /bin/false

        - ansible.builtin.debug:
            msg: Never  executed
      <mark>rescue:</mark>
        - ansible.builtin.debug:
            msg: Caught an error
      <mark>always:</mark>
        - ansible.builtin.debug:
            msg: I always run
</code></pre>

*  Run playbook `error-handling.yml`


#### Summary

* It is sometimes necessary to override or ignore Ansible task conditions
* Failed and changed states can be controlled for arbitrary tasks using
  - `failed_when`
  - `changed_when`
* command modules also have _creates_ and _removes_ attributes to help take
  care of some of the work


#### Stop vagrant

```bash
vagrant halt
vagrant destroy
```



### Cloud Infrastructure


#### Cloud Computing
* a.k.a _Someone else's computer_
* Advantages over _on premise_ datacenters:
    * Greater scalability
      - On demand
      - Deploy/teardown in an instant
    * Cost
      - no need for local datacentres


#### Cattle vs Pets
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Common analogy for comparing traditional on premise hosts to cloud computing [1][2]
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Pets
    - few hosts
    - usually long lived (often years)
    - hand fed
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Cattle
    - many hosts (10s, 100s, 1000s)
    - can be short lived
    - provisioned, configured automatically


#### Infrastructure tools and Cloud
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Automation tools essential for cloud computing
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Scale and speed of cloud computing would not be possible without ability to automate provision and deploy processes


#### Interacting with cloud providers
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Interaction with Cloud providers typically via API client (Python, Ruby, Go,
  etc.)
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Clients/libraries for interacting with cloud platforms _often_ maintained by
  provider

| Cloud | Client |  Python Libraries |
| --- | --- | --- |
| AWS  | `aws` | `awscli` |
| Azure | `az` |   `azure-cli-core, azure-mgmt-{compute,dns,..}` |
| Openstack |  `openstack` | `openstacksdk, python-{openstackclient,...}` |
<!-- .element: class="fragment" data-fragment-index="2" -->


#### Cloud API
![API interaction](img/client-cloud-api.drawio.png "Client Cloud interaction")
  


#### Ansible and cloud computing
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Ansible integrates with a number of cloud platforms
  * AWS
  * Azure
  * Openstack
  * Google
  * etc.
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Under the hood Ansible uses the client libraries to interact with provider


#### Ansible Openstack Modules
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Modules in `openstack.cloud` namespace
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Purpose is interaction with an Openstack cloud
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Usually written and maintained by vendor or community and shipped with
  Ansible

| Module | Description |
| --- |    --- |
| `openstack.cloud.server` | Create/remove a server |
| `openstack.cloud.network` | Create/remove a network |
| `openstack.cloud.subnet` | Create/remove a subnet |
| `openstack.cloud.security_group` | Create/remove a security group |
<!-- .element: class="fragment" data-fragment-index="2" -->


#### Ansible and Openstack
* <!-- .element: class="fragment" data-fragment-index="0" -->
  We will use Ansible to create some cloud infrastructure in Openstack
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Demo but if you would like to follow along you will need to [sign up](https://catalystcloud.nz/signup/) for a Catalyst Cloud account
* <!-- .element: class="fragment" data-fragment-index="2" -->
  All of these steps are similar to what you will need to do with other
  providers


#### Install dependencies
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Need to install vendor libraries so Ansible can interact with Openstack
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Make sure your Python virtualenv is active
  ```
  source ~/venv/bin/activate
  ```
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Install libraries
    ```
    pip install wheel
    pip install -U wheel python-{openstackclient,ceilometerclient,\
      heatclient,neutronclient,swiftclient,octaviaclient,magnumclient} \
      openstacksdk dnspython ansible 
    ```


#### Authenticating with Catalyst Cloud
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Log in to Catalyst Cloud 
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Download the authentication script ![sign up script](img/os-config-download.png "Client auth script") <!-- .element: class="img-right" -->
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Source the script to prompt for login
  ```
  source ./youraccountname-catalystcloud.sh
  ```


#### Ansible playbooks
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Source code for this lesson is in following location
  ```
  cd $INTRO_ANSIBLE_DIR/cloud-provisioning
  ```
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Playbooks for 
  - provisioning hosts, networks in Catalyst Cloud
  - deploying a basic application to hosts
  - tearing down hosts


#### Exploring project
* Let's have a look at the playbooks in this project
* Provision hosts
* deploy code
* Tear down instances


#### Building project
* <!-- .element: class="fragment" data-fragment-index="0" -->
  Provision hosts in Catalyst Cloud
  ```
  ansible-playbook provision-hosts.yml
  ```
* <!-- .element: class="fragment" data-fragment-index="1" -->
  Deploy code on machines
  ```
  ansible-playbook deploy-app.yml
  ```
* <!-- .element: class="fragment" data-fragment-index="2" -->
  Set up local machine 
  ```
  ansible-playbook setup-networking.yml -K
  ```
  <!-- .element: style="font-size:12pt;"  -->


#### Teardown project
* Make sure to delete any infrastructure you have created
  ```
  ansible-playbook remove-hosts.yml -K
  ```


#### Summary
* Cloud infrastructure important component of modern business
* On demand, scalable infrastructure
* Ansible provides tools for managing cloud infrastructure


#### References

1. Baker, B. (2012). _Scaling SQL Server_
2. Bias R. (2014). _The Cloud Revolution_. [https://www.slideshare.net/randybias/the-cloud-revolution-cyber-press-forum-philippines](https://www.slideshare.net/randybias/the-cloud-revolution-cyber-press-forum-philippines)


### Testing Ansible
#### Molecule

[Online Documentation](https://molecule.readthedocs.io/en/latest/)


#### Testing Roles with Molecule

* <!-- .element: style="font-size: 18pt;" -->
  Meant to help while developing new Ansible roles
  * <!-- .element: style="font-size: 16pt;" -->
    *It supports the two latest major versions of Ansible (2.9 and 2.10 currently)*
* <!-- .element: style="font-size: 18pt;" -->
  A shared framework for writing consistent Ansible roles
* <!-- .element: style="font-size: 18pt;" -->
  Allows for testing a wide range of operating systems/distributions
* <!-- .element: style="font-size: 18pt;" -->
  Based on Python and available as a package (pip)
* <!-- .element: style="font-size: 18pt;" -->
  Similar to [vagrant](https://www.vagrantup.com/) but aimed at Ansible
* <!-- .element: style="font-size: 18pt;" -->
  Configuration is stored with the role under the `molecule/` directory inside the role

**But! You can also use it as a playground to learn more about Ansible**
<!-- .element: class="fragment" data-fragment-index="0" style="font-size: 18pt;" -->


#### What can it test?

* <!-- .element: style="font-size: 18pt;" -->
  Molecule uses Ansible as its default verifier, but it also supports
  [Testinfra](https://testinfra.readthedocs.io/en/latest/) which comes with a
  set of built-in [modules](https://testinfra.readthedocs.io/en/latest/modules.html)
  * <!-- .element: class="fragment" data-fragment-index="0" style="font-size: 18pt;" -->
    Verify that files exist with expected permissions and content
  * <!-- .element: class="fragment" data-fragment-index="1" style="font-size: 18pt;" -->
    Verify services are enabled and running
  * <!-- .element: class="fragment" data-fragment-index="2" style="font-size: 18pt;" -->
    Verify users and/or groups are present
  * <!-- .element: class="fragment" data-fragment-index="3" style="font-size: 18pt;" -->
    Verify that packages, and specific package versions, are installed
  * <!-- .element: class="fragment" data-fragment-index="4" style="font-size: 18pt;" -->
    You can also run Ansible modules from within the test as well


#### Terminology

| Term              | Description                                                                            |
|:------------------|:---------------------------------------------------------------------------------------|
| Scenario          | _One or more test suites to apply for the role_                                        |
| Driver/Provider\* | _Defines the type of instance to create (docker, vagrant, etc.)_                       |
| Provisioner       | _How to run role tasks against instance_                                               |
| Converge          | _Apply role tasks as defined by the provisioner_                                       |
| Verifier          | _Framework to use for verifying instance state_                                        |
| Idempotency       | _Make sure that no tasks are showing up as <code style="color:orange;">changed</code>_ |
| Lint              | _Static code analysis for style/syntax checking_                                       |

_*Molecule supports any driver that Ansible supports_
<!-- .element: style="font-size: 14pt;" -->


#### Useful commands

| Command                | Description                                        |
|:-----------------------|:---------------------------------------------------|
| `molecule --help`      | _Get all available sub-commands_                   |
| `molecule init`        | _Create a new role/scenario_                       |
| `molecule list`        | _Output a list of all configured instances_        |
| `molecule create`      | _Create (start) instances associated with_         |
| `molecule converge`    | _Apply the Ansible role to the instances_          |
| `molecule idempotence` | _Verify that no unpexpected changes are happening_ |
| `molecule verify`      | _Run tests (testinfra) against the instances_      |
| `molecule login`       | _Login to a particular instance_                   |
| `molecule destroy`     | _Cleanup_                                          |


#### Exercise: Installation

* <!-- .element: class="fragment" data-fragment-index="0" style="font-size: 18pt;" -->
  [Demo](https://galaxy.ansible.com/kistleh/nginx)
* <!-- .element: class="fragment" data-fragment-index="1" style="font-size: 18pt;" -->
  Installation
  ```bash
  # Install Python dependencies
  sudo apt update && sudo apt install python3-dev python3-venv libssl-dev -y

  # Create a new virtual environment
  python3 -m venv ~/.venvs/molecule
  source ~/.venvs/molecule/bin/activate

  # Install Molecule (which will also install Ansible)
  pip install --upgrade pip
  pip install --upgrade setuptools
  pip install wheel
  pip install "molecule[ansible,docker,lint]"

  # Check version
  molecule --version
  mol --version
  ansible --version

  # Install our skeleton role
  cd $INTRO_ANSIBLE_DIR/ansible-roles/roles
  molecule init role --driver-name docker experimental_role
  cd experimental_role
  ```
* <!-- .element: class="fragment" data-fragment-index="2" style="font-size: 18pt;" -->
  Familiarize yourself with the role layout - Do you see anything missing?


#### Molecule generated roles

```
.
├── defaults
│   └── main.yml
├── files
├── handlers
│   └── main.yml
├── meta
│   └── main.yml
├── molecule               <-- molecule base dir
│   └── default            <-- initial scenario
│       ├── converge.yml
│       ├── molecule.yml
│       └── verify.yml
├── README.md
├── tasks
│   └── main.yml
├── templates
├── tests
│   ├── inventory
│   └── test.yml
└── vars
    └── main.yml

```


#### Molecule Scenario
* <!-- .element: class="fragment" data-fragment-index="0" -->
  In Molecule a _scenario_ is a basic set of tests
* <!-- .element: class="fragment" data-fragment-index="1" -->
  A scenario will test your infrastructure at a particular state
* <!-- .element: class="fragment" data-fragment-index="2" -->
  The role you are developing should get your infrastructure into that state 
* <!-- .element: class="fragment" data-fragment-index="3" -->
  Precise definition of desired state will depend on what your role is intended to do
  - install/remove a package
  - start/stop a service
  - configuration


#### Molecule org
* <!-- .element: class="fragment" data-fragment-index="" -->

* <!-- .element: class="fragment" data-fragment-index="4" -->
  Molecule helps encourage clean Ansible development practices
  - linting syntax
  - idempotence





#### Exercise: Getting started

<div style="width: 40%; float: left; font-size: 18pt;">
  <pre class="fragment" data-fragment-index="0"><code data-trim>
    # Initialize a new default scenario with Testinfra as the verifier
    molecule init scenario -d docker -r kistleh.nginx --verifier-name testinfra default

    # List instances
    molecule list
  </code></pre>
  <pre class="fragment" data-fragment-index="1"><code data-trim>
    ---
    # This is so we don't have to duplicate it for every platform
    default_platform: &platform
      privileged: true
      tmpfs:
        - /run
        - /tmp
      volumes:
        - "/sys/fs/cgroup:/sys/fs/cgroup:ro"
      command: /lib/systemd/systemd
      tty: true
      environment:
        container: docker
    platforms:
      - name: default-ubuntu1804
        image: litmusimage/ubuntu:18.04
        published_ports:
          - "127.0.0.1:10004:80/tcp"
        <<: *platform
  </code></pre>
  <pre class="fragment" data-fragment-index="2"><code data-trim>
    # Verify instance name has changed and create it
    molecule list
    molecule create
    molecule login --host default-ubuntu1804
  </code></pre>
</div>

<div style="width: 60%; float: left; font-size: 14pt;">
  <ul>
    <li class="fragment" data-fragment-index="0">Initialize new scenario</li>
    <li class="fragment" data-fragment-index="1">Update configuration file <code>molecule/default/molecule.yml</code></li>
    <li class="fragment" data-fragment-index="2">Verify instance name has changed and create it</li>
    <li class="fragment" data-fragment-index="3">On your own</li>
    <ul>
      <li class="fragment" data-fragment-index="3">Apply the role and verify you can visit http://localhost:10004</li>
      <li class="fragment" data-fragment-index="3">Verify the role's idempotency (find and fix the bug)</li>
      <li class="fragment" data-fragment-index="3">Tip: <code>molecule --help</code></li>
    </ul>
    <li class="fragment" data-fragment-index="4">Run <code>molecule destroy</code> when you're done</li>
  </ul>
<div>


#### Exercise: Verify role syntax

* <!-- .element: class="fragment" data-fragment-index="0" style="font-size: 18pt;" -->
  ```yaml
  # .yamllint
  ---
  extends: default

  rules:
    line-length:
      max: 120
    new-lines:
      type: unix
  ```
* <!-- .element: class="fragment" data-fragment-index="1" style="font-size: 18pt;" -->
  ```ini
  ; .flake8
  [flake8]
  max-line-length = 120
  ```
* <!-- .element: class="fragment" data-fragment-index="2" style="font-size: 18pt;" -->
  ```yaml
  # molecule/default/molecule.yml
  ...
  lint: |
    set -e
    yamllint .
    ansible-lint .
    flake8
  ...
  ```
* <!-- .element: class="fragment" data-fragment-index="3" style="font-size: 18pt;" -->
  ```bash
  # Check and fix any role lint/syntax issues
  molecule syntax
  molecule lint
  ```


#### Exercise: Add CentOS/Ubuntu instances

* <!-- .element: style="font-size: 18pt;" -->
  Update configuration file `molecule/default/molecule.yml`
  ```yaml
  platforms:
    - name: default-centos7
      image: litmusimage/centos:7
      published_ports:
        - "127.0.0.1:10002:80/tcp"
      <<: *platform

    - name: default-centos8
      image: litmusimage/centos:8
      published_ports:
        - "127.0.0.1:10003:80/tcp"
      <<: *platform

    - name: default-ubuntu1804
      image: litmusimage/ubuntu:18.04
      published_ports:
        - "127.0.0.1:10004:80/tcp"
      <<: *platform

    - name: default-ubuntu2004
      image: litmusimage/ubuntu:20.04
      published_ports:
        - "127.0.0.1:10005:80/tcp"
      <<: *platform
  ```
* <!-- .element: style="font-size: 18pt;" -->
  You should have 4 instances when you're done (`molecule list`)


#### Exercise: Update role for CentOS/Ubuntu

Modify the role to work on both CentOS and Ubuntu
<!-- .element: style="font-size: 18pt;" -->

<div style="width: 50%; float: left; font-size: 18pt;"><pre>
.
├── tasks
│   ├── centos.yml
│   ├── <mark class="fragment" data-fragment-index="1">main.yml</mark>
│   └── ubuntu.yml
└── vars
    ├── CentOS.yml
    ├── main.yml
    └── Ubuntu.yml
</pre></div>

<div class="fragment" data-fragment-index="1" style="width: 50%; float: left; font-size: 18pt;"><pre><code data-trim>
- include_vars:
    file: "{{ ansible_facts.distribution }}.yml"

- import_tasks: centos.yml
  when: ansible_facts.distribution == "CentOS"

- import_tasks: ubuntu.yml
  when: ansible_facts.distribution == "Ubuntu"
</code></pre></div>

_Hint: The installation method and the user/group are **unique** between CentOS and Ubuntu_
<!-- .element: class="fragment" data-fragment-index="2" style="font-size: 14pt;" -->

<div class="fragment" data-fragment-index="3" style="width: 50%; float: left; font-size: 18pt;"><pre><code data-trim data-noescape>
- name: Install EPEL Repository
  yum:
    name: "https://dl.fedoraproject.org/pub/epel/epel-release-latest-{{ ansible_facts.distribution_major_version }}.noarch.rpm"
    state: present
    disable_gpg_check: true
</code></pre></div>

<div class="fragment" data-fragment-index="4" style="width: 50%; float: left; font-size: 14pt;">
  <ul>
    <li><a href="http://localhost:10002">CentOS 7</a></li>
    <li><a href="http://localhost:10003">CentOS 8</a></li>
    <li><a href="http://localhost:10004">Ubuntu 18.04</a></li>
    <li><a href="http://localhost:10005">Ubuntu 20.04</a></li>
  </ul>
</div>


#### Solution: Demo

* Task layout (pay attention to task names)
* `meta/main.yml`


#### Testinfra

* <!-- .element: class="fragment" data-fragment-index="0" style="font-size: 18pt;" -->
  Unit tests for servers
  * Written in Python as a plugin to Pytest
  * Tests are written in Python as well
  * [Online Documentation](https://testinfra.readthedocs.io/en/latest/)
* <!-- .element: class="fragment" data-fragment-index="1" style="font-size: 18pt;" -->
  Tests defined in `molecule/<scenario>/tests/test_*.py`
  * Molecule default test at `molecule/<scenario>/tests/test_default.py`
* <!-- .element: class="fragment" data-fragment-index="2" style="font-size: 18pt;" -->
  A Python function for each type of test that starts with `test_`
* <!-- .element: class="fragment" data-fragment-index="3" style="font-size: 18pt;" -->
  Demo


#### Exercise: Testinfra

* <!-- .element: style="font-size: 14pt;" -->
  Install Testinfra
  ```bash
  pip install pytest-testinfra
  ```
* <!-- .element: style="font-size: 14pt;" -->
  Update `molecule/default/tests/test_default.py` (indentation important)
  ```
  """Role testing files using testinfra."""
  import os
  import sys

  def test_nginx_is_installed(host):
      nginx = host.package("nginx")

      assert nginx.is_installed
  ```

* <!-- .element: style="font-size: 14pt;" -->
  Update `molecule/default/molecule.yml` to be a bit more verbose
  ```
  verifier:
    name: testinfra
    options:
      verbose: true
  ```
* <!-- .element: style="font-size: 14pt;" -->
  Verify that the test pass with `molecule verify`
* <!-- .element: class="fragment" data-fragment-index="0" style="font-size: 14pt;" -->
  Write a new test to verify that Nginx is running and enabled to start at boot
  ```
  def test_nginx_is_running_and_enabled(host):
      # Tests goes here...
  ```


#### Challenge 1: Testinfra

* <!-- .element: style="font-size: 18pt;" -->
  Write a new test function named `test_nginx_configuration` that verifies that
  the Nginx configuration file (`/etc/nginx/nginx.conf`):
  * Exists
  * Is a file
  * Owned by the `root` user
  * Owned by the `root` group
  * Has _any_ expected content (use `molecule login` to check)
* <!-- .element: style="font-size: 18pt;" -->
  Check out the [online documentation](https://testinfra.readthedocs.io/en/latest/)
  for help


#### Challenge 2: Testinfra

* <!-- .element: style="font-size: 18pt;" -->
  Write a new test function named `test_nginx_default_site` that verifies that
  the default site (`/etc/nginx/conf.d/default.conf`):
  * Exists
  * Is a file
  * Owned by `www-data` user/group on Ubuntu systems
  * Owned by `nginx` user/group on CentOS systems
  * Permissions are set to `640`
  * Has _any_ expected content (use `molecule login` to check)
* <!-- .element: style="font-size: 18pt;" -->
  Check out the [online documentation](https://testinfra.readthedocs.io/en/latest/)
  for help


#### Challenge 3: Testinfra

* <!-- .element: style="font-size: 18pt;" -->
  Write a new test function that verifies:
  * Nginx is listening on `0.0.0.0:80`
  * Visiting `http://127.0.0.1:80` returns 200
  * Visiting `http://127.0.0.1:80` returns expected OS content
* <!-- .element: style="font-size: 18pt;" -->
  Check out the [online documentation](https://testinfra.readthedocs.io/en/latest/)
  for help


#### Solution: Testinfra Challenges

* _A solution_
* Full test sequence with `molecule test`


#### Exercise: Ansible Verifier

* <!-- .element: style="font-size: 18pt;" -->
  Initialize a new scenario with `ansible` as the verifier
  ```
  molecule init scenario -d docker -r kistleh.nginx --verifier-name ansible ansible
  molecule list
  molecule create -s ansible
  molecule login
  ```
* <!-- .element: class="fragment" data-fragment-index="0" style="font-size: 18pt;" -->
  On your own
  * Destroy the instance and setup Ubuntu/CentOS instances for the `ansible` scenario
  * Create and converge all instances
  * Re-write the tests you've written with Testinfra to Ansible
    * Use `molecule/ansible/verify.yml`
  * Verify there are no outstanding syntax/lint issues


#### Demo: Ansible Galaxy

* <!-- .element: style="font-size: 18pt;" -->
  Commit changes to GitHub repository
* <!-- .element: style="font-size: 18pt;" -->
  Create a new tag
* <!-- .element: style="font-size: 18pt;" -->
  Re-import role to Ansible Galaxy
  * It should now support multiple operating systems
## The End
### of part 1
