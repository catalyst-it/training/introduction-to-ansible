FROM node:latest

WORKDIR /usr/src/slides

COPY slides .

RUN npm install -g npm@8.13.1
RUN npm install 

EXPOSE 8000
CMD ["npm", "start"]
