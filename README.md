# Introduction to Ansible

This repository includes slides for an Introduction to Ansible course. 


## Course outline
* [Introduction](slides/content.md#introduction-to-ansible)
* [Automation & Infrastructure as Code](slides/content.md#automation-and-infrastructure-as-code)
* [Types of Automation](slides/content.md#types-of-automation)
* [About Ansible](slides/content.md#about-ansible)
* [Installing Ansible](slides/content.md#installing-ansible)
* [Ansible Basics](slides/content.md#ansible-basics)
* [Running Ansible](slides/content.md#running-ansible)
* [Playbooks](slides/content.md#playbooks)
* [Variables in ansible](slides/content.md#variables-in-ansible)
* [Templates](slides/content.md#ansible-templates)
* [Iteration](slides/content.md#iteration-and-conditionals)
* [Conditionals](slides/content.md#conditionals)
* [Protecting Secrets in Ansible](slides/content.md#protecting-secrets-in-ansible)
* [Filters](slides/content.md#filters)
* [Orchestration](slides/content.md#orchestrating-applications)
* [Organising Infrastructure Code](slides/content.md#organising-infrastructure-code) with roles
* [Ansible best practices](slides/content.md#ansible-best-practices)

## Slides

The slides are setup using [reveal.js](https://github.com/hakimel/reveal.js/)
and can be viewed using [Docker](https://www.docker.com/) as shown below.

```
# Build the container
docker build -t ansible-intro-slides:latest .

# Run the docker container and view slides at http://localhost:8000
docker run -p 8000:8000 -d --name intro-slides ansible-intro-slides:latest

# Clean up when you're done
docker rm -f intro-slides
docker rmi ansible-intro-slides:latest
```
